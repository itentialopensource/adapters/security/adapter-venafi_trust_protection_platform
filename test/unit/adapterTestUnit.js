/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-venafi_trust_protection_platform',
      type: 'VenafiTrustProtectionPlatform',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const VenafiTrustProtectionPlatform = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] venafi_trust_protection_platform Adapter Test', () => {
  describe('VenafiTrustProtectionPlatform Class Tests', () => {
    const a = new VenafiTrustProtectionPlatform(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('venafi_trust_protection_platform'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('venafi_trust_protection_platform'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('VenafiTrustProtectionPlatform', pronghornDotJson.export);
          assert.equal('venafi_trust_protection_platform', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-venafi_trust_protection_platform', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('venafi_trust_protection_platform'));
          assert.equal('VenafiTrustProtectionPlatform', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-venafi_trust_protection_platform', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-venafi_trust_protection_platform', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#requestDeviceAuth - errors', () => {
      it('should have a requestDeviceAuth function', (done) => {
        try {
          assert.equal(true, typeof a.requestDeviceAuth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.requestDeviceAuth(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-requestDeviceAuth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authorizeRoot - errors', () => {
      it('should have a authorizeRoot function', (done) => {
        try {
          assert.equal(true, typeof a.authorizeRoot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.authorizeRoot(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-authorizeRoot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authorizeOAuth - errors', () => {
      it('should have a authorizeOAuth function', (done) => {
        try {
          assert.equal(true, typeof a.authorizeOAuth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.authorizeOAuth(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-authorizeOAuth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#integratedAuthorize - errors', () => {
      it('should have a integratedAuthorize function', (done) => {
        try {
          assert.equal(true, typeof a.integratedAuthorize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.integratedAuthorize(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-integratedAuthorize', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateAuthorize - errors', () => {
      it('should have a certificateAuthorize function', (done) => {
        try {
          assert.equal(true, typeof a.certificateAuthorize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.certificateAuthorize(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificateAuthorize', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#token - errors', () => {
      it('should have a token function', (done) => {
        try {
          assert.equal(true, typeof a.token === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.token(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-token', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verifyToken - errors', () => {
      it('should have a verifyToken function', (done) => {
        try {
          assert.equal(true, typeof a.verifyToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeToken - errors', () => {
      it('should have a revokeToken function', (done) => {
        try {
          assert.equal(true, typeof a.revokeToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificates - errors', () => {
      it('should have a getCertificates function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateCount - errors', () => {
      it('should have a getCertificateCount function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificateCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateDetails - errors', () => {
      it('should have a getCertificateDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificateDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.getCertificateDetails(null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getCertificateDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificatePut - errors', () => {
      it('should have a certificatePut function', (done) => {
        try {
          assert.equal(true, typeof a.certificatePut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.certificatePut(null, null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificatePut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.certificatePut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificatePut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateDelete - errors', () => {
      it('should have a certificateDelete function', (done) => {
        try {
          assert.equal(true, typeof a.certificateDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.certificateDelete(null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificateDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reset - errors', () => {
      it('should have a reset function', (done) => {
        try {
          assert.equal(true, typeof a.reset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.reset(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-reset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retry - errors', () => {
      it('should have a retry function', (done) => {
        try {
          assert.equal(true, typeof a.retry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.retry(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-retry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getValidationResults - errors', () => {
      it('should have a getValidationResults function', (done) => {
        try {
          assert.equal(true, typeof a.getValidationResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.getValidationResults(null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getValidationResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateRetrieveByVaultId - errors', () => {
      it('should have a certificateRetrieveByVaultId function', (done) => {
        try {
          assert.equal(true, typeof a.certificateRetrieveByVaultId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vaultId', (done) => {
        try {
          a.certificateRetrieveByVaultId(null, null, (data, error) => {
            try {
              const displayE = 'vaultId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificateRetrieveByVaultId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.certificateRetrieveByVaultId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificateRetrieveByVaultId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateRetrieveByVaultIdGet - errors', () => {
      it('should have a certificateRetrieveByVaultIdGet function', (done) => {
        try {
          assert.equal(true, typeof a.certificateRetrieveByVaultIdGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vaultId', (done) => {
        try {
          a.certificateRetrieveByVaultIdGet(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'vaultId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificateRetrieveByVaultIdGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificatePreviousVersions - errors', () => {
      it('should have a certificatePreviousVersions function', (done) => {
        try {
          assert.equal(true, typeof a.certificatePreviousVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.certificatePreviousVersions(null, null, null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificatePreviousVersions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateValidate - errors', () => {
      it('should have a certificateValidate function', (done) => {
        try {
          assert.equal(true, typeof a.certificateValidate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.certificateValidate(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificateValidate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkPolicy - errors', () => {
      it('should have a checkPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.checkPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.checkPolicy(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-checkPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateRequest - errors', () => {
      it('should have a certificateRequest function', (done) => {
        try {
          assert.equal(true, typeof a.certificateRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.certificateRequest(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificateRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateRetrieve - errors', () => {
      it('should have a certificateRetrieve function', (done) => {
        try {
          assert.equal(true, typeof a.certificateRetrieve === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.certificateRetrieve(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificateRetrieve', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateRetrieveGet - errors', () => {
      it('should have a certificateRetrieveGet function', (done) => {
        try {
          assert.equal(true, typeof a.certificateRetrieveGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateRenew - errors', () => {
      it('should have a certificateRenew function', (done) => {
        try {
          assert.equal(true, typeof a.certificateRenew === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.certificateRenew(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificateRenew', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateRevoke - errors', () => {
      it('should have a certificateRevoke function', (done) => {
        try {
          assert.equal(true, typeof a.certificateRevoke === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.certificateRevoke(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificateRevoke', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateAssociate - errors', () => {
      it('should have a certificateAssociate function', (done) => {
        try {
          assert.equal(true, typeof a.certificateAssociate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.certificateAssociate(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificateAssociate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateDissociate - errors', () => {
      it('should have a certificateDissociate function', (done) => {
        try {
          assert.equal(true, typeof a.certificateDissociate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.certificateDissociate(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificateDissociate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificatePush - errors', () => {
      it('should have a certificatePush function', (done) => {
        try {
          assert.equal(true, typeof a.certificatePush === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.certificatePush(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificatePush', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateImport - errors', () => {
      it('should have a certificateImport function', (done) => {
        try {
          assert.equal(true, typeof a.certificateImport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.certificateImport(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-certificateImport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#create - errors', () => {
      it('should have a create function', (done) => {
        try {
          assert.equal(true, typeof a.create === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.create(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-create', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete - errors', () => {
      it('should have a delete function', (done) => {
        try {
          assert.equal(true, typeof a.delete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.delete(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-delete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enumerate - errors', () => {
      it('should have a enumerate function', (done) => {
        try {
          assert.equal(true, typeof a.enumerate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.enumerate(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-enumerate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rename - errors', () => {
      it('should have a rename function', (done) => {
        try {
          assert.equal(true, typeof a.rename === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.rename(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-rename', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieve - errors', () => {
      it('should have a retrieve function', (done) => {
        try {
          assert.equal(true, typeof a.retrieve === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.retrieve(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-retrieve', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#update - errors', () => {
      it('should have a update function', (done) => {
        try {
          assert.equal(true, typeof a.update === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.update(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-update', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#create2 - errors', () => {
      it('should have a create2 function', (done) => {
        try {
          assert.equal(true, typeof a.create2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.create2(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-create2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#update2 - errors', () => {
      it('should have a update2 function', (done) => {
        try {
          assert.equal(true, typeof a.update2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.update2(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-update2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#create3 - errors', () => {
      it('should have a create3 function', (done) => {
        try {
          assert.equal(true, typeof a.create3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.create3(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-create3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#update3 - errors', () => {
      it('should have a update3 function', (done) => {
        try {
          assert.equal(true, typeof a.update3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.update3(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-update3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#create4 - errors', () => {
      it('should have a create4 function', (done) => {
        try {
          assert.equal(true, typeof a.create4 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorRequest', (done) => {
        try {
          a.create4(null, (data, error) => {
            try {
              const displayE = 'connectorRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-create4', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#read - errors', () => {
      it('should have a read function', (done) => {
        try {
          assert.equal(true, typeof a.read === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.read(null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-read', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#update4 - errors', () => {
      it('should have a update4 function', (done) => {
        try {
          assert.equal(true, typeof a.update4 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.update4(null, null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-update4', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectorRequest', (done) => {
        try {
          a.update4('fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectorRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-update4', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete2 - errors', () => {
      it('should have a delete2 function', (done) => {
        try {
          assert.equal(true, typeof a.delete2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.delete2(null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-delete2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDiscoveryResults - errors', () => {
      it('should have a postDiscoveryResults function', (done) => {
        try {
          assert.equal(true, typeof a.postDiscoveryResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.postDiscoveryResults(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-postDiscoveryResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveryDelete - errors', () => {
      it('should have a discoveryDelete function', (done) => {
        try {
          assert.equal(true, typeof a.discoveryDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.discoveryDelete(null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-discoveryDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#self - errors', () => {
      it('should have a self function', (done) => {
        try {
          assert.equal(true, typeof a.self === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#browse - errors', () => {
      it('should have a browse function', (done) => {
        try {
          assert.equal(true, typeof a.browse === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.browse(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-browse', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssociatedEntries - errors', () => {
      it('should have a getAssociatedEntries function', (done) => {
        try {
          assert.equal(true, typeof a.getAssociatedEntries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.getAssociatedEntries(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getAssociatedEntries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMembers - errors', () => {
      it('should have a getMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.getMembers(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMemberships - errors', () => {
      it('should have a getMemberships function', (done) => {
        try {
          assert.equal(true, typeof a.getMemberships === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.getMemberships(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getMemberships', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readAttribute - errors', () => {
      it('should have a readAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.readAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.readAttribute(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-readAttribute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validate - errors', () => {
      it('should have a validate function', (done) => {
        try {
          assert.equal(true, typeof a.validate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.validate(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-validate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setIdentityPassword - errors', () => {
      it('should have a setIdentityPassword function', (done) => {
        try {
          assert.equal(true, typeof a.setIdentityPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.setIdentityPassword(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-setIdentityPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addGroup - errors', () => {
      it('should have a addGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addGroup(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-addGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#renameGroup - errors', () => {
      it('should have a renameGroup function', (done) => {
        try {
          assert.equal(true, typeof a.renameGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.renameGroup(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-renameGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroup - errors', () => {
      it('should have a deleteGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefix', (done) => {
        try {
          a.deleteGroup(null, null, (data, error) => {
            try {
              const displayE = 'prefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-deleteGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing principal', (done) => {
        try {
          a.deleteGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'principal is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-deleteGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addGroupMembers - errors', () => {
      it('should have a addGroupMembers function', (done) => {
        try {
          assert.equal(true, typeof a.addGroupMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addGroupMembers(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-addGroupMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeGroupMembers - errors', () => {
      it('should have a removeGroupMembers function', (done) => {
        try {
          assert.equal(true, typeof a.removeGroupMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.removeGroupMembers(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-removeGroupMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogsForObject - errors', () => {
      it('should have a getLogsForObject function', (done) => {
        try {
          assert.equal(true, typeof a.getLogsForObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.getLogsForObject(null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getLogsForObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogs - errors', () => {
      it('should have a getLogs function', (done) => {
        try {
          assert.equal(true, typeof a.getLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enumerateObjectsFilteredRecursive - errors', () => {
      it('should have a enumerateObjectsFilteredRecursive function', (done) => {
        try {
          assert.equal(true, typeof a.enumerateObjectsFilteredRecursive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.enumerateObjectsFilteredRecursive(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-enumerateObjectsFilteredRecursive', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#handleGetLogSchema - errors', () => {
      it('should have a handleGetLogSchema function', (done) => {
        try {
          assert.equal(true, typeof a.handleGetLogSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enumerateTickets - errors', () => {
      it('should have a enumerateTickets function', (done) => {
        try {
          assert.equal(true, typeof a.enumerateTickets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.enumerateTickets(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-enumerateTickets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enumerateApproved - errors', () => {
      it('should have a enumerateApproved function', (done) => {
        try {
          assert.equal(true, typeof a.enumerateApproved === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.enumerateApproved(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-enumerateApproved', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#approve - errors', () => {
      it('should have a approve function', (done) => {
        try {
          assert.equal(true, typeof a.approve === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.approve(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-approve', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reject - errors', () => {
      it('should have a reject function', (done) => {
        try {
          assert.equal(true, typeof a.reject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.reject(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-reject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#count - errors', () => {
      it('should have a count function', (done) => {
        try {
          assert.equal(true, typeof a.count === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.count(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-count', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#countApproved - errors', () => {
      it('should have a countApproved function', (done) => {
        try {
          assert.equal(true, typeof a.countApproved === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.countApproved(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-countApproved', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadSingle - errors', () => {
      it('should have a loadSingle function', (done) => {
        try {
          assert.equal(true, typeof a.loadSingle === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.loadSingle(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-loadSingle', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateExisting - errors', () => {
      it('should have a updateExisting function', (done) => {
        try {
          assert.equal(true, typeof a.updateExisting === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.updateExisting(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-updateExisting', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#set - errors', () => {
      it('should have a set function', (done) => {
        try {
          assert.equal(true, typeof a.set === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.set(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-set', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setPolicy - errors', () => {
      it('should have a setPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.setPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.setPolicy(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-setPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#get - errors', () => {
      it('should have a get function', (done) => {
        try {
          assert.equal(true, typeof a.get === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.get(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-get', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readEffective - errors', () => {
      it('should have a readEffective function', (done) => {
        try {
          assert.equal(true, typeof a.readEffective === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.readEffective(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-readEffective', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readPolicy - errors', () => {
      it('should have a readPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.readPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.readPolicy(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-readPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getItems - errors', () => {
      it('should have a getItems function', (done) => {
        try {
          assert.equal(true, typeof a.getItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.getItems(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyItems - errors', () => {
      it('should have a getPolicyItems function', (done) => {
        try {
          assert.equal(true, typeof a.getPolicyItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.getPolicyItems(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getPolicyItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getItemsForClass - errors', () => {
      it('should have a getItemsForClass function', (done) => {
        try {
          assert.equal(true, typeof a.getItemsForClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.getItemsForClass(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getItemsForClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getItemGuids - errors', () => {
      it('should have a getItemGuids function', (done) => {
        try {
          assert.equal(true, typeof a.getItemGuids === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.getItemGuids(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getItemGuids', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#find - errors', () => {
      it('should have a find function', (done) => {
        try {
          assert.equal(true, typeof a.find === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.find(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-find', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#defineItem - errors', () => {
      it('should have a defineItem function', (done) => {
        try {
          assert.equal(true, typeof a.defineItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.defineItem(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-defineItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#undefineItem - errors', () => {
      it('should have a undefineItem function', (done) => {
        try {
          assert.equal(true, typeof a.undefineItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.undefineItem(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-undefineItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findItem - errors', () => {
      it('should have a findItem function', (done) => {
        try {
          assert.equal(true, typeof a.findItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.findItem(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-findItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadItem - errors', () => {
      it('should have a loadItem function', (done) => {
        try {
          assert.equal(true, typeof a.loadItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.loadItem(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-loadItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadItemGuid - errors', () => {
      it('should have a loadItemGuid function', (done) => {
        try {
          assert.equal(true, typeof a.loadItemGuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.loadItemGuid(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-loadItemGuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateItem - errors', () => {
      it('should have a updateItem function', (done) => {
        try {
          assert.equal(true, typeof a.updateItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.updateItem(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-updateItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllItems - errors', () => {
      it('should have a getAllItems function', (done) => {
        try {
          assert.equal(true, typeof a.getAllItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enforceRoles - errors', () => {
      it('should have a enforceRoles function', (done) => {
        try {
          assert.equal(true, typeof a.enforceRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.enforceRoles(null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-enforceRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getByGuid - errors', () => {
      it('should have a getByGuid function', (done) => {
        try {
          assert.equal(true, typeof a.getByGuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.getByGuid(null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getByGuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#update5 - errors', () => {
      it('should have a update5 function', (done) => {
        try {
          assert.equal(true, typeof a.update5 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.update5(null, null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-update5', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.update5('fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-update5', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete3 - errors', () => {
      it('should have a delete3 function', (done) => {
        try {
          assert.equal(true, typeof a.delete3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.delete3(null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-delete3', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enrollAndProvision - errors', () => {
      it('should have a enrollAndProvision function', (done) => {
        try {
          assert.equal(true, typeof a.enrollAndProvision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.enrollAndProvision(null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-enrollAndProvision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#create5 - errors', () => {
      it('should have a create5 function', (done) => {
        try {
          assert.equal(true, typeof a.create5 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.create5(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-create5', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#get2 - errors', () => {
      it('should have a get2 function', (done) => {
        try {
          assert.equal(true, typeof a.get2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#create6 - errors', () => {
      it('should have a create6 function', (done) => {
        try {
          assert.equal(true, typeof a.create6 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.create6(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-create6', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete4 - errors', () => {
      it('should have a delete4 function', (done) => {
        try {
          assert.equal(true, typeof a.delete4 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.delete4(null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-delete4', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getByGuid2 - errors', () => {
      it('should have a getByGuid2 function', (done) => {
        try {
          assert.equal(true, typeof a.getByGuid2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.getByGuid2(null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getByGuid2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#update6 - errors', () => {
      it('should have a update6 function', (done) => {
        try {
          assert.equal(true, typeof a.update6 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.update6(null, null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-update6', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.update6('fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-update6', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getKeysetDetail - errors', () => {
      it('should have a getKeysetDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getKeysetDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keysetId', (done) => {
        try {
          a.getKeysetDetail(null, null, (data, error) => {
            try {
              const displayE = 'keysetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getKeysetDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loadKeyData', (done) => {
        try {
          a.getKeysetDetail('fakeparam', null, (data, error) => {
            try {
              const displayE = 'loadKeyData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getKeysetDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getKeysetDetails - errors', () => {
      it('should have a getKeysetDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getKeysetDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing generatedKeysetFilterPageSizeOffsetLoadKeyData', (done) => {
        try {
          a.getKeysetDetails(null, (data, error) => {
            try {
              const displayE = 'generatedKeysetFilterPageSizeOffsetLoadKeyData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getKeysetDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testDeviceConnection - errors', () => {
      it('should have a testDeviceConnection function', (done) => {
        try {
          assert.equal(true, typeof a.testDeviceConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing generateddeviceGuids', (done) => {
        try {
          a.testDeviceConnection(null, (data, error) => {
            try {
              const displayE = 'generateddeviceGuids is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-testDeviceConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importKeyUsageData - errors', () => {
      it('should have a importKeyUsageData function', (done) => {
        try {
          assert.equal(true, typeof a.importKeyUsageData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.importKeyUsageData(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-importKeyUsageData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getKeyUsages - errors', () => {
      it('should have a getKeyUsages function', (done) => {
        try {
          assert.equal(true, typeof a.getKeyUsages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing generatedSshKeyUsageFilterPageSizeOffset', (done) => {
        try {
          a.getKeyUsages(null, (data, error) => {
            try {
              const displayE = 'generatedSshKeyUsageFilterPageSizeOffset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getKeyUsages', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remove - errors', () => {
      it('should have a remove function', (done) => {
        try {
          assert.equal(true, typeof a.remove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyRequest', (done) => {
        try {
          a.remove(null, (data, error) => {
            try {
              const displayE = 'keyRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-remove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelKeyOperation - errors', () => {
      it('should have a cancelKeyOperation function', (done) => {
        try {
          assert.equal(true, typeof a.cancelKeyOperation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyRequest', (done) => {
        try {
          a.cancelKeyOperation(null, (data, error) => {
            try {
              const displayE = 'keyRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-cancelKeyOperation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retryKeyOperation - errors', () => {
      it('should have a retryKeyOperation function', (done) => {
        try {
          assert.equal(true, typeof a.retryKeyOperation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyRequest', (done) => {
        try {
          a.retryKeyOperation(null, (data, error) => {
            try {
              const displayE = 'keyRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-retryKeyOperation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startRotation - errors', () => {
      it('should have a startRotation function', (done) => {
        try {
          assert.equal(true, typeof a.startRotation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keysetRequest', (done) => {
        try {
          a.startRotation(null, (data, error) => {
            try {
              const displayE = 'keysetRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-startRotation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelRotation - errors', () => {
      it('should have a cancelRotation function', (done) => {
        try {
          assert.equal(true, typeof a.cancelRotation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keysetRequest', (done) => {
        try {
          a.cancelRotation(null, (data, error) => {
            try {
              const displayE = 'keysetRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-cancelRotation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retryRotation - errors', () => {
      it('should have a retryRotation function', (done) => {
        try {
          assert.equal(true, typeof a.retryRotation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keysetRequest', (done) => {
        try {
          a.retryRotation(null, (data, error) => {
            try {
              const displayE = 'keysetRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-retryRotation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editKeyOptions - errors', () => {
      it('should have a editKeyOptions function', (done) => {
        try {
          assert.equal(true, typeof a.editKeyOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionsRequest', (done) => {
        try {
          a.editKeyOptions(null, (data, error) => {
            try {
              const displayE = 'optionsRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-editKeyOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editSelfServiceAuthorizedKey - errors', () => {
      it('should have a editSelfServiceAuthorizedKey function', (done) => {
        try {
          assert.equal(true, typeof a.editSelfServiceAuthorizedKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.editSelfServiceAuthorizedKey(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-editSelfServiceAuthorizedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSelfServicePrivateKey - errors', () => {
      it('should have a addSelfServicePrivateKey function', (done) => {
        try {
          assert.equal(true, typeof a.addSelfServicePrivateKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addSelfServicePrivateKey(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-addSelfServicePrivateKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSelfServiceAuthorizedKey - errors', () => {
      it('should have a addSelfServiceAuthorizedKey function', (done) => {
        try {
          assert.equal(true, typeof a.addSelfServiceAuthorizedKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addSelfServiceAuthorizedKey(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-addSelfServiceAuthorizedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#confirmSelfServiceKeyInstallation - errors', () => {
      it('should have a confirmSelfServiceKeyInstallation function', (done) => {
        try {
          assert.equal(true, typeof a.confirmSelfServiceKeyInstallation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keysetRequest', (done) => {
        try {
          a.confirmSelfServiceKeyInstallation(null, (data, error) => {
            try {
              const displayE = 'keysetRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-confirmSelfServiceKeyInstallation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#moveKeysetsToPolicy - errors', () => {
      it('should have a moveKeysetsToPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.moveKeysetsToPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing migrationRequest', (done) => {
        try {
          a.moveKeysetsToPolicy(null, (data, error) => {
            try {
              const displayE = 'migrationRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-moveKeysetsToPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportSelfServiceAuthorizedKey - errors', () => {
      it('should have a exportSelfServiceAuthorizedKey function', (done) => {
        try {
          assert.equal(true, typeof a.exportSelfServiceAuthorizedKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.exportSelfServiceAuthorizedKey(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-exportSelfServiceAuthorizedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportSelfServicePrivateKey - errors', () => {
      it('should have a exportSelfServicePrivateKey function', (done) => {
        try {
          assert.equal(true, typeof a.exportSelfServicePrivateKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.exportSelfServicePrivateKey(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-exportSelfServicePrivateKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#skipKeyRotation - errors', () => {
      it('should have a skipKeyRotation function', (done) => {
        try {
          assert.equal(true, typeof a.skipKeyRotation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyRequest', (done) => {
        try {
          a.skipKeyRotation(null, (data, error) => {
            try {
              const displayE = 'keyRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-skipKeyRotation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUserPrivateKey - errors', () => {
      it('should have a addUserPrivateKey function', (done) => {
        try {
          assert.equal(true, typeof a.addUserPrivateKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addUserPrivateKey(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-addUserPrivateKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addHostPrivateKey - errors', () => {
      it('should have a addHostPrivateKey function', (done) => {
        try {
          assert.equal(true, typeof a.addHostPrivateKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addHostPrivateKey(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-addHostPrivateKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAuthorizedKey - errors', () => {
      it('should have a addAuthorizedKey function', (done) => {
        try {
          assert.equal(true, typeof a.addAuthorizedKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addAuthorizedKey(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-addAuthorizedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addKnownHostKey - errors', () => {
      it('should have a addKnownHostKey function', (done) => {
        try {
          assert.equal(true, typeof a.addKnownHostKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addKnownHostKey(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-addKnownHostKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importAuthorizedKey - errors', () => {
      it('should have a importAuthorizedKey function', (done) => {
        try {
          assert.equal(true, typeof a.importAuthorizedKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.importAuthorizedKey(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-importAuthorizedKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importPrivateKey - errors', () => {
      it('should have a importPrivateKey function', (done) => {
        try {
          assert.equal(true, typeof a.importPrivateKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.importPrivateKey(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-importPrivateKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changePrivateKeyPassphrase - errors', () => {
      it('should have a changePrivateKeyPassphrase function', (done) => {
        try {
          assert.equal(true, typeof a.changePrivateKeyPassphrase === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.changePrivateKeyPassphrase(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-changePrivateKeyPassphrase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setUnmatchedKeysetPasshrase - errors', () => {
      it('should have a setUnmatchedKeysetPasshrase function', (done) => {
        try {
          assert.equal(true, typeof a.setUnmatchedKeysetPasshrase === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.setUnmatchedKeysetPasshrase(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-setUnmatchedKeysetPasshrase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUnmatchedKeyset - errors', () => {
      it('should have a deleteUnmatchedKeyset function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUnmatchedKeyset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestData', (done) => {
        try {
          a.deleteUnmatchedKeyset(null, (data, error) => {
            try {
              const displayE = 'requestData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-deleteUnmatchedKeyset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getKeyDetails - errors', () => {
      it('should have a getKeyDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getKeyDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing generatedKeyId', (done) => {
        try {
          a.getKeyDetails(null, (data, error) => {
            try {
              const displayE = 'generatedKeyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getKeyDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevices - errors', () => {
      it('should have a getDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing generatedSshDeviceFilterPageSizeOffset', (done) => {
        try {
          a.getDevices(null, (data, error) => {
            try {
              const displayE = 'generatedSshDeviceFilterPageSizeOffset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#approveKeyOperation - errors', () => {
      it('should have a approveKeyOperation function', (done) => {
        try {
          assert.equal(true, typeof a.approveKeyOperation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing approveRequest', (done) => {
        try {
          a.approveKeyOperation(null, (data, error) => {
            try {
              const displayE = 'approveRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-approveKeyOperation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rejectKeyOperation - errors', () => {
      it('should have a rejectKeyOperation function', (done) => {
        try {
          assert.equal(true, typeof a.rejectKeyOperation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rejectRequest', (done) => {
        try {
          a.rejectKeyOperation(null, (data, error) => {
            try {
              const displayE = 'rejectRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-rejectKeyOperation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stats - errors', () => {
      it('should have a stats function', (done) => {
        try {
          assert.equal(true, typeof a.stats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupBy', (done) => {
        try {
          a.stats(null, (data, error) => {
            try {
              const displayE = 'groupBy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-stats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#policyViolations - errors', () => {
      it('should have a policyViolations function', (done) => {
        try {
          assert.equal(true, typeof a.policyViolations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#criticalAlerts - errors', () => {
      it('should have a criticalAlerts function', (done) => {
        try {
          assert.equal(true, typeof a.criticalAlerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing minAllowedKeyLength', (done) => {
        try {
          a.criticalAlerts(null, (data, error) => {
            try {
              const displayE = 'minAllowedKeyLength is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-criticalAlerts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#requestCertificate - errors', () => {
      it('should have a requestCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.requestCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.requestCertificate(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-requestCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveCertificate - errors', () => {
      it('should have a retrieveCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.retrieveCertificate(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-retrieveCertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvailableTemplates - errors', () => {
      it('should have a getAvailableTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getAvailableTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveTemplate - errors', () => {
      it('should have a retrieveTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.retrieveTemplate(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-retrieveTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveCAPublicKeyData - errors', () => {
      it('should have a retrieveCAPublicKeyData function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveCAPublicKeyData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing guid', (done) => {
        try {
          a.retrieveCAPublicKeyData(null, null, (data, error) => {
            try {
              const displayE = 'guid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-retrieveCAPublicKeyData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dn', (done) => {
        try {
          a.retrieveCAPublicKeyData('fakeparam', null, (data, error) => {
            try {
              const displayE = 'dn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-retrieveCAPublicKeyData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNewCAKeypair - errors', () => {
      it('should have a createNewCAKeypair function', (done) => {
        try {
          assert.equal(true, typeof a.createNewCAKeypair === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.createNewCAKeypair(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-createNewCAKeypair', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeam - errors', () => {
      it('should have a getTeam function', (done) => {
        try {
          assert.equal(true, typeof a.getTeam === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefix', (done) => {
        try {
          a.getTeam(null, null, (data, error) => {
            try {
              const displayE = 'prefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing principal', (done) => {
        try {
          a.getTeam('fakeparam', null, (data, error) => {
            try {
              const displayE = 'principal is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-getTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTeam - errors', () => {
      it('should have a updateTeam function', (done) => {
        try {
          assert.equal(true, typeof a.updateTeam === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.updateTeam(null, null, null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-updateTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefix', (done) => {
        try {
          a.updateTeam('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'prefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-updateTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing principal', (done) => {
        try {
          a.updateTeam('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'principal is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-updateTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTeam - errors', () => {
      it('should have a deleteTeam function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTeam === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing prefix', (done) => {
        try {
          a.deleteTeam(null, null, (data, error) => {
            try {
              const displayE = 'prefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-deleteTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing principal', (done) => {
        try {
          a.deleteTeam('fakeparam', null, (data, error) => {
            try {
              const displayE = 'principal is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-deleteTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTeam - errors', () => {
      it('should have a createTeam function', (done) => {
        try {
          assert.equal(true, typeof a.createTeam === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.createTeam(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-createTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addTeamMembers - errors', () => {
      it('should have a addTeamMembers function', (done) => {
        try {
          assert.equal(true, typeof a.addTeamMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addTeamMembers(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-addTeamMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeTeamMembers - errors', () => {
      it('should have a removeTeamMembers function', (done) => {
        try {
          assert.equal(true, typeof a.removeTeamMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.removeTeamMembers(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-removeTeamMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addTeamOwners - errors', () => {
      it('should have a addTeamOwners function', (done) => {
        try {
          assert.equal(true, typeof a.addTeamOwners === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.addTeamOwners(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-addTeamOwners', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#demoteTeamOwners - errors', () => {
      it('should have a demoteTeamOwners function', (done) => {
        try {
          assert.equal(true, typeof a.demoteTeamOwners === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.demoteTeamOwners(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-demoteTeamOwners', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#renameTeam - errors', () => {
      it('should have a renameTeam function', (done) => {
        try {
          assert.equal(true, typeof a.renameTeam === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.renameTeam(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-renameTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ticketCreate - errors', () => {
      it('should have a ticketCreate function', (done) => {
        try {
          assert.equal(true, typeof a.ticketCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.ticketCreate(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-ticketCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ticketDelete - errors', () => {
      it('should have a ticketDelete function', (done) => {
        try {
          assert.equal(true, typeof a.ticketDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.ticketDelete(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-ticketDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ticketDetails - errors', () => {
      it('should have a ticketDetails function', (done) => {
        try {
          assert.equal(true, typeof a.ticketDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.ticketDetails(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-ticketDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ticketEnumerate - errors', () => {
      it('should have a ticketEnumerate function', (done) => {
        try {
          assert.equal(true, typeof a.ticketEnumerate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.ticketEnumerate(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-ticketEnumerate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ticketExists - errors', () => {
      it('should have a ticketExists function', (done) => {
        try {
          assert.equal(true, typeof a.ticketExists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.ticketExists(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-ticketExists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ticketStatus - errors', () => {
      it('should have a ticketStatus function', (done) => {
        try {
          assert.equal(true, typeof a.ticketStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.ticketStatus(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-ticketStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ticketUpdateStatus - errors', () => {
      it('should have a ticketUpdateStatus function', (done) => {
        try {
          assert.equal(true, typeof a.ticketUpdateStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.ticketUpdateStatus(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-ticketUpdateStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#get3 - errors', () => {
      it('should have a get3 function', (done) => {
        try {
          assert.equal(true, typeof a.get3 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#add - errors', () => {
      it('should have a add function', (done) => {
        try {
          assert.equal(true, typeof a.add === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.add(null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-venafi_trust_protection_platform-adapter-add', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clear - errors', () => {
      it('should have a clear function', (done) => {
        try {
          assert.equal(true, typeof a.clear === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
