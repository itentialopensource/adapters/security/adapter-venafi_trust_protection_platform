/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-venafi_trust_protection_platform',
      type: 'VenafiTrustProtectionPlatform',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const VenafiTrustProtectionPlatform = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] venafi_trust_protection_platform Adapter Test', () => {
  describe('VenafiTrustProtectionPlatform Class Tests', () => {
    const a = new VenafiTrustProtectionPlatform(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const authorizeAuthorizeRootBodyParam = {
      username: 'string',
      password: 'string',
      client_id: 'string',
      scope: 'string',
      state: 'string',
      redirect_uri: 'string'
    };
    describe('#authorizeRoot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authorizeRoot(authorizeAuthorizeRootBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authorize', 'authorizeRoot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizeCertificateAuthorizeBodyParam = {
      username: 'string',
      password: 'string',
      client_id: 'string',
      scope: 'string',
      state: 'string',
      redirect_uri: 'string'
    };
    describe('#certificateAuthorize - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificateAuthorize(authorizeCertificateAuthorizeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authorize', 'certificateAuthorize', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizeRequestDeviceAuthBodyParam = {
      username: 'string',
      password: 'string',
      client_id: 'string',
      scope: 'string',
      state: 'string',
      redirect_uri: 'string'
    };
    describe('#requestDeviceAuth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.requestDeviceAuth(authorizeRequestDeviceAuthBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authorize', 'requestDeviceAuth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizeIntegratedAuthorizeBodyParam = {
      username: 'string',
      password: 'string',
      client_id: 'string',
      scope: 'string',
      state: 'string',
      redirect_uri: 'string'
    };
    describe('#integratedAuthorize - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.integratedAuthorize(authorizeIntegratedAuthorizeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authorize', 'integratedAuthorize', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizeAuthorizeOAuthBodyParam = {
      username: 'string',
      password: 'string',
      client_id: 'string',
      scope: 'string',
      state: 'string',
      redirect_uri: 'string'
    };
    describe('#authorizeOAuth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authorizeOAuth(authorizeAuthorizeOAuthBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authorize', 'authorizeOAuth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authorizeTokenBodyParam = {
      refresh_token: 'string',
      client_id: 'string',
      grant_type: 'string',
      device_code: 'string'
    };
    describe('#token - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.token(authorizeTokenBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authorize', 'token', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verifyToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.verifyToken((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authorize', 'verifyToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeToken((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authorize', 'revokeToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesCertificateAssociateBodyParam = {
      CertificateDN: 'string',
      PushToNew: true,
      ApplicationDN: [
        'string'
      ]
    };
    describe('#certificateAssociate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificateAssociate(certificatesCertificateAssociateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificateAssociate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesCheckPolicyBodyParam = {
      PolicyDN: 'string',
      PKCS10: 'string'
    };
    describe('#checkPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.checkPolicy(certificatesCheckPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'checkPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesCertificateDissociateBodyParam = {
      CertificateDN: 'string',
      ApplicationDN: [
        'string'
      ],
      DeleteOrphans: false
    };
    describe('#certificateDissociate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificateDissociate(certificatesCertificateDissociateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificateDissociate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesCertificateImportBodyParam = {
      PolicyDN: 'string',
      ObjectName: 'string',
      CertificateData: 'string',
      PrivateKeyData: 'string',
      Password: 'string',
      CASpecificAttributes: [
        [
          {}
        ]
      ],
      Reconcile: false
    };
    describe('#certificateImport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificateImport(certificatesCertificateImportBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificateImport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesCertificatePushBodyParam = {
      CertificateDN: 'string',
      ApplicationDN: [
        'string'
      ],
      PushToAll: false
    };
    describe('#certificatePush - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificatePush(certificatesCertificatePushBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificatePush', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesCertificateRenewBodyParam = {
      CertificateDN: 'string',
      PKCS10: 'string',
      Reenable: true,
      WorkToDoTimeout: 7,
      Format: 'string',
      Password: 'string',
      IncludePrivateKey: false,
      IncludeChain: false,
      FriendlyName: 'string',
      RootFirstOrder: true,
      KeystorePassword: 'string'
    };
    describe('#certificateRenew - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificateRenew(certificatesCertificateRenewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificateRenew', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesCertificateRequestBodyParam = {
      PolicyDN: 'string',
      CADN: 'string',
      ObjectName: 'string',
      Subject: 'string',
      OrganizationalUnit: 'string',
      Organization: 'string',
      City: 'string',
      State: 'string',
      Country: 'string',
      SubjectAltNames: [
        {
          Type: 2,
          Name: 'string',
          TypeName: 'string'
        }
      ],
      Contacts: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 3,
          Disabled: false
        }
      ],
      Approvers: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 6,
          Disabled: false
        }
      ],
      CASpecificAttributes: [
        [
          {}
        ]
      ],
      PKCS10: 'string',
      KeyBitSize: 3,
      DisableAutomaticRenewal: false,
      ManagementType: 'string',
      SetWorkToDo: true,
      WorkToDoTimeout: 5,
      Devices: [
        {
          PolicyDN: 'string',
          ObjectName: 'string',
          Description: 'string',
          Contacts: [
            {
              Prefix: 'string',
              PrefixedName: 'string',
              PrefixedUniversal: 'string',
              Name: 'string',
              FullName: 'string',
              Universal: 'string',
              IsGroup: false,
              Type: 9,
              Disabled: false
            }
          ],
          Host: 'string',
          Port: 5,
          CredentialDN: 'string',
          TempDirectory: 'string',
          UseSudo: false,
          SudoCredentialDN: 'string',
          ConcurrentConnectionLimit: 9,
          TrustedFingerprint: 'string',
          EnforceKnownHost: false,
          CreatedBy: 'string',
          Applications: [
            {
              Description: 'string',
              ObjectName: 'string',
              DriverName: 'string',
              Class: 'string',
              Approvers: [
                {
                  Prefix: 'string',
                  PrefixedName: 'string',
                  PrefixedUniversal: 'string',
                  Name: 'string',
                  FullName: 'string',
                  Universal: 'string',
                  IsGroup: false,
                  Type: 10,
                  Disabled: true
                }
              ],
              Contacts: [
                {
                  Prefix: 'string',
                  PrefixedName: 'string',
                  PrefixedUniversal: 'string',
                  Name: 'string',
                  FullName: 'string',
                  Universal: 'string',
                  IsGroup: false,
                  Type: 8,
                  Disabled: true
                }
              ],
              CreatedBy: 'string',
              PrivateKeyCredentialDN: 'string',
              DisableValidation: false,
              DisableNetworkValidation: true,
              ValidationHost: 'string',
              ValidationPort: 2,
              ClassSpecificAttributes: [
                [
                  {}
                ]
              ]
            }
          ],
          CloudInstanceID: 'string',
          CloudRegion: 'string',
          CloudService: 'string'
        }
      ],
      CreatedBy: 'string',
      KeyAlgorithm: 'string',
      EllipticCurve: 'string',
      Reenable: true,
      CertificateType: 'string',
      CustomFields: [
        {
          Name: 'string',
          Values: [
            'string'
          ]
        }
      ],
      Origin: 'string',
      Format: 'string',
      Password: 'string',
      IncludePrivateKey: true,
      IncludeChain: true,
      FriendlyName: 'string',
      RootFirstOrder: false,
      KeystorePassword: 'string'
    };
    describe('#certificateRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificateRequest(certificatesCertificateRequestBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificateRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesResetBodyParam = {
      CertificateDN: 'string',
      Restart: false,
      WorkToDoTimeout: 8
    };
    describe('#reset - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reset(certificatesResetBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'reset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesCertificateRetrieveBodyParam = {
      CertificateDN: 'string',
      Format: 'string',
      Password: 'string',
      IncludePrivateKey: false,
      IncludeChain: false,
      FriendlyName: 'string',
      RootFirstOrder: false,
      KeystorePassword: 'string',
      WorkToDoTimeout: 3
    };
    describe('#certificateRetrieve - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificateRetrieve(certificatesCertificateRetrieveBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificateRetrieve', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesVaultId = 'fakedata';
    const certificatesCertificateRetrieveByVaultIdBodyParam = {
      Format: 'string',
      Password: 'string',
      IncludePrivateKey: false,
      IncludeChain: false,
      FriendlyName: 'string',
      RootFirstOrder: false,
      KeystorePassword: 'string'
    };
    describe('#certificateRetrieveByVaultId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificateRetrieveByVaultId(certificatesVaultId, certificatesCertificateRetrieveByVaultIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificateRetrieveByVaultId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesRetryBodyParam = {
      CertificateDN: 'string',
      WorkToDoTimeout: 3
    };
    describe('#retry - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retry(certificatesRetryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'retry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesCertificateRevokeBodyParam = {
      CertificateDN: 'string',
      Thumbprint: 'string',
      Reason: 9,
      Comments: 'string',
      Disable: false,
      WorkToDoTimeout: 9
    };
    describe('#certificateRevoke - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificateRevoke(certificatesCertificateRevokeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificateRevoke', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesCertificateValidateBodyParam = {
      CertificateDNs: [
        'string'
      ],
      CertificateGUIDs: [
        'string'
      ]
    };
    describe('#certificateValidate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificateValidate(certificatesCertificateValidateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificateValidate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCertificates(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'getCertificates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCertificateCount(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'getCertificateCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateRetrieveGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificateRetrieveGet(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificateRetrieveGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateRetrieveByVaultIdGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificateRetrieveByVaultIdGet(certificatesVaultId, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificateRetrieveByVaultIdGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const certificatesGuid = 'fakedata';
    const certificatesCertificatePutBodyParam = {
      AttributeData: [
        [
          {}
        ]
      ]
    };
    describe('#certificatePut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificatePut(certificatesGuid, certificatesCertificatePutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificatePut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCertificateDetails(certificatesGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'getCertificateDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificatePreviousVersions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificatePreviousVersions(certificatesGuid, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificatePreviousVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getValidationResults - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getValidationResults(certificatesGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'getValidationResults', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsCreate3BodyParam = {
      CustomFields: [
        [
          {}
        ]
      ],
      ConnectorName: 'string',
      CredentialType: 'string',
      CredentialPath: 'string'
    };
    describe('#create3 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.create3(credentialsCreate3BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'create3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsUpdate3BodyParam = {
      CustomFields: [
        [
          {}
        ]
      ],
      ConnectorName: 'string',
      CredentialType: 'string',
      CredentialPath: 'string'
    };
    describe('#update3 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.update3(credentialsUpdate3BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'update3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsCreate4BodyParam = {
      ConnectorName: 'string',
      ServiceAddress: 'string',
      ServiceCredential: 'string',
      PowershellScript: 'string',
      AllowedIdentities: [
        'string'
      ],
      Description: 'string'
    };
    describe('#create4 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.create4(credentialsCreate4BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'create4', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsCreate2BodyParam = {
      CyberArkUsername: 'string',
      CyberArkPassword: 'string',
      Username: 'string',
      AppID: 'string',
      SafeName: 'string',
      FolderName: 'string',
      AccountName: 'string',
      Description: 'string',
      EncryptionKey: 'string',
      Expiration: 'string',
      Contact: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 3,
          Disabled: true
        }
      ],
      Shared: true,
      FriendlyName: 'string',
      Classname: 'string',
      Values: [
        {
          Name: 'string',
          Type: 'string',
          Value: 'string'
        }
      ],
      CredentialPath: 'string',
      NewCredentialPath: 'string',
      Pattern: 'string',
      Recursive: false
    };
    describe('#create2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.create2(credentialsCreate2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'create2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsUpdate2BodyParam = {
      CyberArkUsername: 'string',
      CyberArkPassword: 'string',
      Username: 'string',
      AppID: 'string',
      SafeName: 'string',
      FolderName: 'string',
      AccountName: 'string',
      Description: 'string',
      EncryptionKey: 'string',
      Expiration: 'string',
      Contact: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 10,
          Disabled: false
        }
      ],
      Shared: false,
      FriendlyName: 'string',
      Classname: 'string',
      Values: [
        {
          Name: 'string',
          Type: 'string',
          Value: 'string'
        }
      ],
      CredentialPath: 'string',
      NewCredentialPath: 'string',
      Pattern: 'string',
      Recursive: false
    };
    describe('#update2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.update2(credentialsUpdate2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'update2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsCreateBodyParam = {
      Description: 'string',
      EncryptionKey: 'string',
      Expiration: 'string',
      Contact: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 3,
          Disabled: true
        }
      ],
      Shared: true,
      FriendlyName: 'string',
      Classname: 'string',
      Values: [
        {
          Name: 'string',
          Type: 'string',
          Value: 'string'
        }
      ],
      CredentialPath: 'string',
      NewCredentialPath: 'string',
      Pattern: 'string',
      Recursive: false
    };
    describe('#create - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.create(credentialsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'create', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsDeleteBodyParam = {
      Description: 'string',
      EncryptionKey: 'string',
      Expiration: 'string',
      Contact: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 7,
          Disabled: false
        }
      ],
      Shared: true,
      FriendlyName: 'string',
      Classname: 'string',
      Values: [
        {
          Name: 'string',
          Type: 'string',
          Value: 'string'
        }
      ],
      CredentialPath: 'string',
      NewCredentialPath: 'string',
      Pattern: 'string',
      Recursive: true
    };
    describe('#delete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete(credentialsDeleteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'delete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsEnumerateBodyParam = {
      Description: 'string',
      EncryptionKey: 'string',
      Expiration: 'string',
      Contact: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 10,
          Disabled: false
        }
      ],
      Shared: false,
      FriendlyName: 'string',
      Classname: 'string',
      Values: [
        {
          Name: 'string',
          Type: 'string',
          Value: 'string'
        }
      ],
      CredentialPath: 'string',
      NewCredentialPath: 'string',
      Pattern: 'string',
      Recursive: true
    };
    describe('#enumerate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enumerate(credentialsEnumerateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'enumerate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsRenameBodyParam = {
      Description: 'string',
      EncryptionKey: 'string',
      Expiration: 'string',
      Contact: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 6,
          Disabled: true
        }
      ],
      Shared: false,
      FriendlyName: 'string',
      Classname: 'string',
      Values: [
        {
          Name: 'string',
          Type: 'string',
          Value: 'string'
        }
      ],
      CredentialPath: 'string',
      NewCredentialPath: 'string',
      Pattern: 'string',
      Recursive: true
    };
    describe('#rename - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rename(credentialsRenameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'rename', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsRetrieveBodyParam = {
      Description: 'string',
      EncryptionKey: 'string',
      Expiration: 'string',
      Contact: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 9,
          Disabled: true
        }
      ],
      Shared: false,
      FriendlyName: 'string',
      Classname: 'string',
      Values: [
        {
          Name: 'string',
          Type: 'string',
          Value: 'string'
        }
      ],
      CredentialPath: 'string',
      NewCredentialPath: 'string',
      Pattern: 'string',
      Recursive: true
    };
    describe('#retrieve - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieve(credentialsRetrieveBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'retrieve', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsUpdateBodyParam = {
      Description: 'string',
      EncryptionKey: 'string',
      Expiration: 'string',
      Contact: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 2,
          Disabled: false
        }
      ],
      Shared: true,
      FriendlyName: 'string',
      Classname: 'string',
      Values: [
        {
          Name: 'string',
          Type: 'string',
          Value: 'string'
        }
      ],
      CredentialPath: 'string',
      NewCredentialPath: 'string',
      Pattern: 'string',
      Recursive: false
    };
    describe('#update - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.update(credentialsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'update', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsGuid = 'fakedata';
    const credentialsUpdate4BodyParam = {
      ConnectorName: 'string',
      ServiceAddress: 'string',
      ServiceCredential: 'string',
      PowershellScript: 'string',
      AllowedIdentities: [
        'string'
      ],
      Description: 'string'
    };
    describe('#update4 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.update4(credentialsGuid, credentialsUpdate4BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'update4', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#read - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.read(credentialsGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'read', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const discoveryPostDiscoveryResultsBodyParam = {
      endpoints: [
        {
          ip: 'string',
          host: 'string',
          port: 5,
          certificates: [
            {
              certificate: 'string',
              fingerprint: 'string'
            }
          ]
        }
      ],
      zoneName: 'string'
    };
    describe('#postDiscoveryResults - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postDiscoveryResults(discoveryPostDiscoveryResultsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Discovery', 'postDiscoveryResults', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identitySetIdentityPasswordBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 5,
        Disabled: true
      },
      ResolveNested: 9,
      AttributeName: 'string',
      Container: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 10,
        Disabled: false
      },
      Filter: 'string',
      Limit: 9,
      IdentityType: 7,
      Password: 'string',
      OldPassword: 'string',
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 9,
        Disabled: false
      },
      Group: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 9,
        Disabled: true
      },
      NewGroupName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 3,
          Disabled: false
        }
      ],
      ShowMembers: false
    };
    describe('#setIdentityPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setIdentityPassword(identitySetIdentityPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'setIdentityPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityAddGroupBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 2,
        Disabled: true
      },
      ResolveNested: 3,
      AttributeName: 'string',
      Container: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 3,
        Disabled: true
      },
      Filter: 'string',
      Limit: 5,
      IdentityType: 8,
      Password: 'string',
      OldPassword: 'string',
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 6,
        Disabled: true
      },
      Group: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 10,
        Disabled: true
      },
      NewGroupName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 4,
          Disabled: false
        }
      ],
      ShowMembers: true
    };
    describe('#addGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addGroup(identityAddGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'addGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityBrowseBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 5,
        Disabled: false
      },
      ResolveNested: 7,
      AttributeName: 'string',
      Container: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 5,
        Disabled: true
      },
      Filter: 'string',
      Limit: 6,
      IdentityType: 6,
      Password: 'string',
      OldPassword: 'string',
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 10,
        Disabled: false
      },
      Group: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 3,
        Disabled: true
      },
      NewGroupName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 9,
          Disabled: false
        }
      ],
      ShowMembers: true
    };
    describe('#browse - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.browse(identityBrowseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'browse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityGetAssociatedEntriesBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 3,
        Disabled: false
      },
      ResolveNested: 10,
      AttributeName: 'string',
      Container: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 8,
        Disabled: true
      },
      Filter: 'string',
      Limit: 8,
      IdentityType: 9,
      Password: 'string',
      OldPassword: 'string',
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 1,
        Disabled: true
      },
      Group: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 8,
        Disabled: true
      },
      NewGroupName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 1,
          Disabled: false
        }
      ],
      ShowMembers: false
    };
    describe('#getAssociatedEntries - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAssociatedEntries(identityGetAssociatedEntriesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'getAssociatedEntries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityGetMembersBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 9,
        Disabled: false
      },
      ResolveNested: 2,
      AttributeName: 'string',
      Container: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 10,
        Disabled: false
      },
      Filter: 'string',
      Limit: 7,
      IdentityType: 7,
      Password: 'string',
      OldPassword: 'string',
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 1,
        Disabled: true
      },
      Group: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 7,
        Disabled: false
      },
      NewGroupName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 5,
          Disabled: false
        }
      ],
      ShowMembers: true
    };
    describe('#getMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMembers(identityGetMembersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'getMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityGetMembershipsBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 1,
        Disabled: true
      },
      ResolveNested: 5,
      AttributeName: 'string',
      Container: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 8,
        Disabled: true
      },
      Filter: 'string',
      Limit: 7,
      IdentityType: 9,
      Password: 'string',
      OldPassword: 'string',
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 7,
        Disabled: true
      },
      Group: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 9,
        Disabled: false
      },
      NewGroupName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 2,
          Disabled: false
        }
      ],
      ShowMembers: false
    };
    describe('#getMemberships - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMemberships(identityGetMembershipsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'getMemberships', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityReadAttributeBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 9,
        Disabled: true
      },
      ResolveNested: 4,
      AttributeName: 'string',
      Container: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 8,
        Disabled: true
      },
      Filter: 'string',
      Limit: 2,
      IdentityType: 4,
      Password: 'string',
      OldPassword: 'string',
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 2,
        Disabled: false
      },
      Group: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 2,
        Disabled: true
      },
      NewGroupName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 6,
          Disabled: false
        }
      ],
      ShowMembers: true
    };
    describe('#readAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.readAttribute(identityReadAttributeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'readAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityValidateBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 2,
        Disabled: true
      },
      ResolveNested: 9,
      AttributeName: 'string',
      Container: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 4,
        Disabled: false
      },
      Filter: 'string',
      Limit: 9,
      IdentityType: 1,
      Password: 'string',
      OldPassword: 'string',
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 8,
        Disabled: true
      },
      Group: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 1,
        Disabled: true
      },
      NewGroupName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 6,
          Disabled: true
        }
      ],
      ShowMembers: true
    };
    describe('#validate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.validate(identityValidateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'validate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityAddGroupMembersBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 9,
        Disabled: true
      },
      ResolveNested: 9,
      AttributeName: 'string',
      Container: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 4,
        Disabled: true
      },
      Filter: 'string',
      Limit: 7,
      IdentityType: 10,
      Password: 'string',
      OldPassword: 'string',
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 9,
        Disabled: false
      },
      Group: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 3,
        Disabled: false
      },
      NewGroupName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 10,
          Disabled: false
        }
      ],
      ShowMembers: true
    };
    describe('#addGroupMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addGroupMembers(identityAddGroupMembersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'addGroupMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityRemoveGroupMembersBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 3,
        Disabled: false
      },
      ResolveNested: 6,
      AttributeName: 'string',
      Container: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 5,
        Disabled: true
      },
      Filter: 'string',
      Limit: 2,
      IdentityType: 1,
      Password: 'string',
      OldPassword: 'string',
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 6,
        Disabled: false
      },
      Group: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 8,
        Disabled: false
      },
      NewGroupName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 6,
          Disabled: true
        }
      ],
      ShowMembers: true
    };
    describe('#removeGroupMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeGroupMembers(identityRemoveGroupMembersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'removeGroupMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityRenameGroupBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 4,
        Disabled: true
      },
      ResolveNested: 1,
      AttributeName: 'string',
      Container: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 5,
        Disabled: true
      },
      Filter: 'string',
      Limit: 1,
      IdentityType: 4,
      Password: 'string',
      OldPassword: 'string',
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 7,
        Disabled: true
      },
      Group: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 3,
        Disabled: true
      },
      NewGroupName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 5,
          Disabled: false
        }
      ],
      ShowMembers: true
    };
    describe('#renameGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.renameGroup(identityRenameGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'renameGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#self - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.self((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'self', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const logEnumerateObjectsFilteredRecursiveBodyParam = {
      Component: 'string',
      ID: 1,
      Severity: 6,
      Text1: 'string',
      Text2: 'string',
      Value1: 8,
      Value2: 7,
      Grouping: 7,
      Flags: 7
    };
    describe('#enumerateObjectsFilteredRecursive - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enumerateObjectsFilteredRecursive(logEnumerateObjectsFilteredRecursiveBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Log', 'enumerateObjectsFilteredRecursive', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLogs((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Log', 'getLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#handleGetLogSchema - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleGetLogSchema((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Log', 'handleGetLogSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const logGuid = 'fakedata';
    describe('#getLogsForObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLogsForObject(logGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Log', 'getLogsForObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowTicketsApproveBodyParam = {
      ProductCode: 2,
      TicketPageSize: 10,
      TicketPageNumber: 10,
      Comment: 'string',
      UseCount: 10,
      NotBefore: 'string',
      Expires: 'string',
      TicketIds: [
        4
      ],
      TicketId: 1,
      RejectionLevel: 8,
      Ticket: {
        Id: 1,
        RequiredApprovals: 7,
        CreationTime: 'string',
        RemainingUses: 7,
        NotBefore: 'string',
        ExpiresOn: 'string',
        Approvers: [
          'string'
        ],
        Approvals: [
          {
            Universal: 'string',
            Comment: 'string',
            ApprovalTime: 'string'
          }
        ],
        FlowProcessId: 2,
        Identifier: 'string',
        ProductCode: 4,
        Environment: {},
        PriorEnvironment: {},
        RejectionStatus: 7,
        RejectionComment: 'string'
      }
    };
    describe('#approve - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.approve(flowTicketsApproveBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowTickets', 'approve', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowTicketsCountBodyParam = {
      ProductCode: 3,
      TicketPageSize: 5,
      TicketPageNumber: 3,
      Comment: 'string',
      UseCount: 7,
      NotBefore: 'string',
      Expires: 'string',
      TicketIds: [
        10
      ],
      TicketId: 7,
      RejectionLevel: 7,
      Ticket: {
        Id: 8,
        RequiredApprovals: 5,
        CreationTime: 'string',
        RemainingUses: 9,
        NotBefore: 'string',
        ExpiresOn: 'string',
        Approvers: [
          'string'
        ],
        Approvals: [
          {
            Universal: 'string',
            Comment: 'string',
            ApprovalTime: 'string'
          }
        ],
        FlowProcessId: 3,
        Identifier: 'string',
        ProductCode: 9,
        Environment: {},
        PriorEnvironment: {},
        RejectionStatus: 5,
        RejectionComment: 'string'
      }
    };
    describe('#count - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.count(flowTicketsCountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowTickets', 'count', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowTicketsCountApprovedBodyParam = {
      ProductCode: 5,
      TicketPageSize: 1,
      TicketPageNumber: 8,
      Comment: 'string',
      UseCount: 3,
      NotBefore: 'string',
      Expires: 'string',
      TicketIds: [
        10
      ],
      TicketId: 1,
      RejectionLevel: 7,
      Ticket: {
        Id: 3,
        RequiredApprovals: 1,
        CreationTime: 'string',
        RemainingUses: 1,
        NotBefore: 'string',
        ExpiresOn: 'string',
        Approvers: [
          'string'
        ],
        Approvals: [
          {
            Universal: 'string',
            Comment: 'string',
            ApprovalTime: 'string'
          }
        ],
        FlowProcessId: 7,
        Identifier: 'string',
        ProductCode: 7,
        Environment: {},
        PriorEnvironment: {},
        RejectionStatus: 8,
        RejectionComment: 'string'
      }
    };
    describe('#countApproved - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.countApproved(flowTicketsCountApprovedBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowTickets', 'countApproved', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowTicketsEnumerateTicketsBodyParam = {
      ProductCode: 7,
      TicketPageSize: 7,
      TicketPageNumber: 8,
      Comment: 'string',
      UseCount: 4,
      NotBefore: 'string',
      Expires: 'string',
      TicketIds: [
        8
      ],
      TicketId: 3,
      RejectionLevel: 1,
      Ticket: {
        Id: 4,
        RequiredApprovals: 9,
        CreationTime: 'string',
        RemainingUses: 1,
        NotBefore: 'string',
        ExpiresOn: 'string',
        Approvers: [
          'string'
        ],
        Approvals: [
          {
            Universal: 'string',
            Comment: 'string',
            ApprovalTime: 'string'
          }
        ],
        FlowProcessId: 10,
        Identifier: 'string',
        ProductCode: 1,
        Environment: {},
        PriorEnvironment: {},
        RejectionStatus: 3,
        RejectionComment: 'string'
      }
    };
    describe('#enumerateTickets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enumerateTickets(flowTicketsEnumerateTicketsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowTickets', 'enumerateTickets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowTicketsEnumerateApprovedBodyParam = {
      ProductCode: 3,
      TicketPageSize: 8,
      TicketPageNumber: 4,
      Comment: 'string',
      UseCount: 10,
      NotBefore: 'string',
      Expires: 'string',
      TicketIds: [
        10
      ],
      TicketId: 6,
      RejectionLevel: 8,
      Ticket: {
        Id: 6,
        RequiredApprovals: 2,
        CreationTime: 'string',
        RemainingUses: 2,
        NotBefore: 'string',
        ExpiresOn: 'string',
        Approvers: [
          'string'
        ],
        Approvals: [
          {
            Universal: 'string',
            Comment: 'string',
            ApprovalTime: 'string'
          }
        ],
        FlowProcessId: 4,
        Identifier: 'string',
        ProductCode: 4,
        Environment: {},
        PriorEnvironment: {},
        RejectionStatus: 3,
        RejectionComment: 'string'
      }
    };
    describe('#enumerateApproved - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enumerateApproved(flowTicketsEnumerateApprovedBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowTickets', 'enumerateApproved', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowTicketsLoadSingleBodyParam = {
      ProductCode: 2,
      TicketPageSize: 4,
      TicketPageNumber: 9,
      Comment: 'string',
      UseCount: 3,
      NotBefore: 'string',
      Expires: 'string',
      TicketIds: [
        8
      ],
      TicketId: 4,
      RejectionLevel: 3,
      Ticket: {
        Id: 8,
        RequiredApprovals: 6,
        CreationTime: 'string',
        RemainingUses: 1,
        NotBefore: 'string',
        ExpiresOn: 'string',
        Approvers: [
          'string'
        ],
        Approvals: [
          {
            Universal: 'string',
            Comment: 'string',
            ApprovalTime: 'string'
          }
        ],
        FlowProcessId: 8,
        Identifier: 'string',
        ProductCode: 6,
        Environment: {},
        PriorEnvironment: {},
        RejectionStatus: 10,
        RejectionComment: 'string'
      }
    };
    describe('#loadSingle - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.loadSingle(flowTicketsLoadSingleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowTickets', 'loadSingle', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowTicketsRejectBodyParam = {
      ProductCode: 6,
      TicketPageSize: 6,
      TicketPageNumber: 8,
      Comment: 'string',
      UseCount: 9,
      NotBefore: 'string',
      Expires: 'string',
      TicketIds: [
        1
      ],
      TicketId: 6,
      RejectionLevel: 5,
      Ticket: {
        Id: 1,
        RequiredApprovals: 6,
        CreationTime: 'string',
        RemainingUses: 1,
        NotBefore: 'string',
        ExpiresOn: 'string',
        Approvers: [
          'string'
        ],
        Approvals: [
          {
            Universal: 'string',
            Comment: 'string',
            ApprovalTime: 'string'
          }
        ],
        FlowProcessId: 7,
        Identifier: 'string',
        ProductCode: 5,
        Environment: {},
        PriorEnvironment: {},
        RejectionStatus: 7,
        RejectionComment: 'string'
      }
    };
    describe('#reject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reject(flowTicketsRejectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowTickets', 'reject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowTicketsUpdateExistingBodyParam = {
      ProductCode: 5,
      TicketPageSize: 1,
      TicketPageNumber: 7,
      Comment: 'string',
      UseCount: 3,
      NotBefore: 'string',
      Expires: 'string',
      TicketIds: [
        2
      ],
      TicketId: 8,
      RejectionLevel: 1,
      Ticket: {
        Id: 8,
        RequiredApprovals: 9,
        CreationTime: 'string',
        RemainingUses: 6,
        NotBefore: 'string',
        ExpiresOn: 'string',
        Approvers: [
          'string'
        ],
        Approvals: [
          {
            Universal: 'string',
            Comment: 'string',
            ApprovalTime: 'string'
          }
        ],
        FlowProcessId: 9,
        Identifier: 'string',
        ProductCode: 10,
        Environment: {},
        PriorEnvironment: {},
        RejectionStatus: 5,
        RejectionComment: 'string'
      }
    };
    describe('#updateExisting - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateExisting(flowTicketsUpdateExistingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FlowTickets', 'updateExisting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataDefineItemBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 5,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: true,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 5,
            MaximumLength: 6,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: false,
            DateOnly: false,
            TimeOnly: false,
            Policyable: false,
            ConfigAttribute: 'string',
            RenderHidden: true,
            RenderReadOnly: false
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 4,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: false,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 4,
        MaximumLength: 7,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: true,
        DateOnly: true,
        TimeOnly: false,
        Policyable: true,
        ConfigAttribute: 'string',
        RenderHidden: false,
        RenderReadOnly: false
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: false,
      Type: 'string',
      All: false,
      RemoveData: false,
      KeepExisting: true,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#defineItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.defineItem(metadataDefineItemBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'defineItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataFindBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 3,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: false,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 6,
            MaximumLength: 7,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: true,
            DateOnly: true,
            TimeOnly: false,
            Policyable: false,
            ConfigAttribute: 'string',
            RenderHidden: true,
            RenderReadOnly: false
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 9,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: true,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 2,
        MaximumLength: 1,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: true,
        DateOnly: false,
        TimeOnly: true,
        Policyable: false,
        ConfigAttribute: 'string',
        RenderHidden: false,
        RenderReadOnly: true
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: false,
      Type: 'string',
      All: true,
      RemoveData: false,
      KeepExisting: false,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#find - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.find(metadataFindBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'find', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataFindItemBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 2,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: true,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 9,
            MaximumLength: 1,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: true,
            DateOnly: false,
            TimeOnly: true,
            Policyable: true,
            ConfigAttribute: 'string',
            RenderHidden: false,
            RenderReadOnly: false
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 9,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: true,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 10,
        MaximumLength: 8,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: false,
        DateOnly: false,
        TimeOnly: true,
        Policyable: true,
        ConfigAttribute: 'string',
        RenderHidden: true,
        RenderReadOnly: false
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: false,
      Type: 'string',
      All: true,
      RemoveData: true,
      KeepExisting: true,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#findItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.findItem(metadataFindItemBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'findItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataGetBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 8,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: false,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 3,
            MaximumLength: 3,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: true,
            DateOnly: false,
            TimeOnly: true,
            Policyable: false,
            ConfigAttribute: 'string',
            RenderHidden: false,
            RenderReadOnly: true
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 6,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: false,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 3,
        MaximumLength: 6,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: false,
        DateOnly: true,
        TimeOnly: false,
        Policyable: false,
        ConfigAttribute: 'string',
        RenderHidden: false,
        RenderReadOnly: true
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: false,
      Type: 'string',
      All: true,
      RemoveData: false,
      KeepExisting: true,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#get - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.get(metadataGetBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'get', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataGetItemGuidsBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 5,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: false,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 9,
            MaximumLength: 6,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: true,
            DateOnly: true,
            TimeOnly: false,
            Policyable: true,
            ConfigAttribute: 'string',
            RenderHidden: false,
            RenderReadOnly: true
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 1,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: true,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 4,
        MaximumLength: 5,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: true,
        DateOnly: false,
        TimeOnly: true,
        Policyable: true,
        ConfigAttribute: 'string',
        RenderHidden: false,
        RenderReadOnly: false
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: true,
      Type: 'string',
      All: true,
      RemoveData: true,
      KeepExisting: false,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#getItemGuids - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getItemGuids(metadataGetItemGuidsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'getItemGuids', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataGetItemsBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 6,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: true,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 6,
            MaximumLength: 2,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: true,
            DateOnly: false,
            TimeOnly: true,
            Policyable: true,
            ConfigAttribute: 'string',
            RenderHidden: true,
            RenderReadOnly: false
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 1,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: true,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 7,
        MaximumLength: 9,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: false,
        DateOnly: true,
        TimeOnly: false,
        Policyable: true,
        ConfigAttribute: 'string',
        RenderHidden: false,
        RenderReadOnly: true
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: true,
      Type: 'string',
      All: false,
      RemoveData: false,
      KeepExisting: false,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#getItems - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getItems(metadataGetItemsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'getItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataGetItemsForClassBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 8,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: false,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 6,
            MaximumLength: 6,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: true,
            DateOnly: true,
            TimeOnly: false,
            Policyable: false,
            ConfigAttribute: 'string',
            RenderHidden: false,
            RenderReadOnly: true
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 9,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: false,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 9,
        MaximumLength: 3,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: true,
        DateOnly: true,
        TimeOnly: true,
        Policyable: false,
        ConfigAttribute: 'string',
        RenderHidden: true,
        RenderReadOnly: false
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: false,
      Type: 'string',
      All: false,
      RemoveData: true,
      KeepExisting: false,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#getItemsForClass - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getItemsForClass(metadataGetItemsForClassBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'getItemsForClass', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataGetPolicyItemsBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 5,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: false,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 1,
            MaximumLength: 1,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: false,
            DateOnly: true,
            TimeOnly: false,
            Policyable: true,
            ConfigAttribute: 'string',
            RenderHidden: false,
            RenderReadOnly: true
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 8,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: false,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 9,
        MaximumLength: 4,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: true,
        DateOnly: true,
        TimeOnly: false,
        Policyable: true,
        ConfigAttribute: 'string',
        RenderHidden: false,
        RenderReadOnly: true
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: false,
      Type: 'string',
      All: false,
      RemoveData: true,
      KeepExisting: false,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#getPolicyItems - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPolicyItems(metadataGetPolicyItemsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'getPolicyItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataLoadItemBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 2,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: true,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 9,
            MaximumLength: 6,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: false,
            DateOnly: false,
            TimeOnly: false,
            Policyable: true,
            ConfigAttribute: 'string',
            RenderHidden: false,
            RenderReadOnly: true
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 5,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: false,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 4,
        MaximumLength: 7,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: true,
        DateOnly: true,
        TimeOnly: false,
        Policyable: false,
        ConfigAttribute: 'string',
        RenderHidden: false,
        RenderReadOnly: true
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: false,
      Type: 'string',
      All: true,
      RemoveData: false,
      KeepExisting: false,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#loadItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.loadItem(metadataLoadItemBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'loadItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataLoadItemGuidBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 8,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: true,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 2,
            MaximumLength: 1,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: true,
            DateOnly: false,
            TimeOnly: false,
            Policyable: true,
            ConfigAttribute: 'string',
            RenderHidden: false,
            RenderReadOnly: true
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 9,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: true,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 3,
        MaximumLength: 3,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: false,
        DateOnly: true,
        TimeOnly: true,
        Policyable: false,
        ConfigAttribute: 'string',
        RenderHidden: true,
        RenderReadOnly: false
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: false,
      Type: 'string',
      All: true,
      RemoveData: true,
      KeepExisting: true,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#loadItemGuid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.loadItemGuid(metadataLoadItemGuidBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'loadItemGuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataReadEffectiveBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 4,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: true,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 7,
            MaximumLength: 2,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: false,
            DateOnly: true,
            TimeOnly: true,
            Policyable: false,
            ConfigAttribute: 'string',
            RenderHidden: true,
            RenderReadOnly: false
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 8,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: false,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 10,
        MaximumLength: 2,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: true,
        DateOnly: false,
        TimeOnly: false,
        Policyable: true,
        ConfigAttribute: 'string',
        RenderHidden: true,
        RenderReadOnly: false
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: true,
      Type: 'string',
      All: true,
      RemoveData: false,
      KeepExisting: true,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#readEffective - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.readEffective(metadataReadEffectiveBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'readEffective', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataReadPolicyBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 4,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: true,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 1,
            MaximumLength: 7,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: true,
            DateOnly: false,
            TimeOnly: true,
            Policyable: true,
            ConfigAttribute: 'string',
            RenderHidden: false,
            RenderReadOnly: true
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 8,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: true,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 7,
        MaximumLength: 6,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: true,
        DateOnly: true,
        TimeOnly: true,
        Policyable: true,
        ConfigAttribute: 'string',
        RenderHidden: false,
        RenderReadOnly: true
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: true,
      Type: 'string',
      All: true,
      RemoveData: false,
      KeepExisting: false,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#readPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.readPolicy(metadataReadPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'readPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataSetBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 2,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: false,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 4,
            MaximumLength: 9,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: true,
            DateOnly: false,
            TimeOnly: false,
            Policyable: true,
            ConfigAttribute: 'string',
            RenderHidden: false,
            RenderReadOnly: false
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 2,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: false,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 10,
        MaximumLength: 4,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: false,
        DateOnly: true,
        TimeOnly: false,
        Policyable: false,
        ConfigAttribute: 'string',
        RenderHidden: false,
        RenderReadOnly: false
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: false,
      Type: 'string',
      All: false,
      RemoveData: false,
      KeepExisting: true,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#set - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.set(metadataSetBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'set', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataSetPolicyBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 7,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: true,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 7,
            MaximumLength: 4,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: false,
            DateOnly: false,
            TimeOnly: true,
            Policyable: true,
            ConfigAttribute: 'string',
            RenderHidden: false,
            RenderReadOnly: true
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 3,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: false,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 7,
        MaximumLength: 5,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: true,
        DateOnly: false,
        TimeOnly: false,
        Policyable: true,
        ConfigAttribute: 'string',
        RenderHidden: true,
        RenderReadOnly: true
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: false,
      Type: 'string',
      All: false,
      RemoveData: true,
      KeepExisting: true,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#setPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setPolicy(metadataSetPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'setPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataUndefineItemBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 6,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: true,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 6,
            MaximumLength: 10,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: false,
            DateOnly: false,
            TimeOnly: false,
            Policyable: true,
            ConfigAttribute: 'string',
            RenderHidden: true,
            RenderReadOnly: false
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 2,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: false,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 7,
        MaximumLength: 6,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: true,
        DateOnly: true,
        TimeOnly: false,
        Policyable: true,
        ConfigAttribute: 'string',
        RenderHidden: false,
        RenderReadOnly: false
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: false,
      Type: 'string',
      All: true,
      RemoveData: false,
      KeepExisting: true,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#undefineItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.undefineItem(metadataUndefineItemBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'undefineItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metadataUpdateItemBodyParam = {
      Dataset: [
        {
          Item: {
            Type: 3,
            Label: 'string',
            Help: 'string',
            Category: 'string',
            DisplayAfter: 'string',
            Name: 'string',
            Guid: 'string',
            DN: 'string',
            Mandatory: true,
            DefaultValues: [
              'string'
            ],
            AllowedValues: [
              'string'
            ],
            Classes: [
              'string'
            ],
            LocalizationSet: [
              {
                Language: 'string',
                List: [
                  {
                    Invariant: 'string',
                    Localized: 'string'
                  }
                ]
              }
            ],
            MinimumLength: 10,
            MaximumLength: 10,
            Mask: 'string',
            AllowedCharacters: 'string',
            RegularExpression: 'string',
            ErrorMessage: 'string',
            Single: true,
            DateOnly: false,
            TimeOnly: false,
            Policyable: true,
            ConfigAttribute: 'string',
            RenderHidden: false,
            RenderReadOnly: false
          },
          Values: [
            'string'
          ]
        }
      ],
      GuidData: [
        {
          ItemGuid: 'string',
          List: [
            'string'
          ]
        }
      ],
      Location: 'string',
      Item: {
        Type: 9,
        Label: 'string',
        Help: 'string',
        Category: 'string',
        DisplayAfter: 'string',
        Name: 'string',
        Guid: 'string',
        DN: 'string',
        Mandatory: true,
        DefaultValues: [
          'string'
        ],
        AllowedValues: [
          'string'
        ],
        Classes: [
          'string'
        ],
        LocalizationSet: [
          {
            Language: 'string',
            List: [
              {
                Invariant: 'string',
                Localized: 'string'
              }
            ]
          }
        ],
        MinimumLength: 8,
        MaximumLength: 4,
        Mask: 'string',
        AllowedCharacters: 'string',
        RegularExpression: 'string',
        ErrorMessage: 'string',
        Single: true,
        DateOnly: true,
        TimeOnly: false,
        Policyable: false,
        ConfigAttribute: 'string',
        RenderHidden: false,
        RenderReadOnly: true
      },
      ItemGuid: 'string',
      DN: 'string',
      Name: 'string',
      Value: 'string',
      ConfigClass: 'string',
      Locked: false,
      Type: 'string',
      All: false,
      RemoveData: true,
      KeepExisting: true,
      Update: {
        DN: 'string',
        Data: [
          {
            Name: 'string',
            Value: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#updateItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateItem(metadataUpdateItemBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'updateItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllItems - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllItems((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metadata', 'getAllItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pkiHashicorpCreate5BodyParam = {
      FolderDn: 'string',
      PkiPath: 'string',
      Certificate: {
        CommonName: 'string',
        Organization: 'string',
        OrganizationalUnits: [
          'string'
        ],
        City: 'string',
        State: 'string',
        Country: 'string',
        SubjectAltNames: [
          {
            Name: 'string',
            TypeName: 'string'
          }
        ],
        KeyBitSize: 'string',
        KeyAlgorithm: 'string'
      },
      Installation: {
        Host: 'string',
        Port: 7,
        CredentialDn: 'string'
      },
      CRLAddress: 'string',
      OCSPAddress: 'string',
      CreateCertificateAuthority: false,
      CreatePKIRole: false,
      Roles: [
        'string'
      ]
    };
    describe('#create5 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.create5(pkiHashicorpCreate5BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PkiHashicorp', 'create5', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pkiHashicorpGuid = 'fakedata';
    describe('#enforceRoles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enforceRoles(pkiHashicorpGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PkiHashicorp', 'enforceRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enrollAndProvision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enrollAndProvision(pkiHashicorpGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PkiHashicorp', 'enrollAndProvision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pkiHashicorpCreate6BodyParam = {
      FolderDn: 'string',
      RoleName: 'string',
      WhitelistedDomains: [
        'string'
      ],
      OrganizationalUnits: [
        'string'
      ],
      Organization: 'string',
      City: 'string',
      State: 'string',
      Country: 'string',
      KeyAlgorithm: 'string',
      KeyBitSize: 'string',
      EnhancedKeyUsage: [
        'string'
      ]
    };
    describe('#create6 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.create6(pkiHashicorpCreate6BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PkiHashicorp', 'create6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#get2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.get2((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PkiHashicorp', 'get2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pkiHashicorpUpdate5BodyParam = {
      Certificate: {
        CommonName: 'string',
        Organization: 'string',
        OrganizationalUnits: [
          'string'
        ],
        City: 'string',
        State: 'string',
        Country: 'string',
        SubjectAltNames: [
          {
            Name: 'string',
            TypeName: 'string'
          }
        ],
        KeyBitSize: 'string',
        KeyAlgorithm: 'string'
      },
      Installation: {
        CredentialDn: 'string',
        Port: 2
      },
      CRLAddress: 'string',
      OCSPAddress: 'string',
      CreateCertificateAuthority: false,
      CreatePKIRole: false,
      Roles: [
        'string'
      ]
    };
    describe('#update5 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.update5(pkiHashicorpGuid, pkiHashicorpUpdate5BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PkiHashicorp', 'update5', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getByGuid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getByGuid(pkiHashicorpGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PkiHashicorp', 'getByGuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pkiHashicorpUpdate6BodyParam = {
      WhitelistedDomains: [
        'string'
      ],
      OrganizationalUnits: [
        'string'
      ],
      Organization: 'string',
      City: 'string',
      State: 'string',
      Country: 'string',
      KeyAlgorithm: 'string',
      KeyBitSize: 'string',
      EnhancedKeyUsage: [
        'string'
      ]
    };
    describe('#update6 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.update6(pkiHashicorpGuid, pkiHashicorpUpdate6BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PkiHashicorp', 'update6', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getByGuid2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getByGuid2(pkiHashicorpGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PkiHashicorp', 'getByGuid2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshAddAuthorizedKeyBodyParam = {
      DeviceGuid: 'string',
      Username: 'string',
      Filepath: 'string',
      KeysetId: 'string',
      AllowedSourceRestriction: [
        'string'
      ],
      DeniedSourceRestriction: [
        'string'
      ],
      ForcedCommand: 'string',
      Options: [
        'string'
      ],
      Format: 'string',
      TrailingComment: 'string'
    };
    describe('#addAuthorizedKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addAuthorizedKey(sshAddAuthorizedKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'addAuthorizedKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshAddHostPrivateKeyBodyParam = {
      DeviceGuid: 'string',
      Username: 'string',
      Filepath: 'string',
      KeysetId: 'string',
      Format: 'string'
    };
    describe('#addHostPrivateKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addHostPrivateKey(sshAddHostPrivateKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'addHostPrivateKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshAddKnownHostKeyBodyParam = {
      DeviceGuid: 'string',
      Username: 'string',
      Filepath: 'string',
      KeysetId: 'string',
      Format: 'string'
    };
    describe('#addKnownHostKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addKnownHostKey(sshAddKnownHostKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'addKnownHostKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshAddSelfServiceAuthorizedKeyBodyParam = {
      Owner: 'string',
      ContactEmail: 'string',
      FolderId: 'string',
      Location: 'string',
      Notes: 'string',
      KeysetId: 'string',
      AllowedSourceRestriction: [
        'string'
      ],
      DeniedSourceRestriction: [
        'string'
      ],
      ForcedCommand: 'string',
      Options: [
        'string'
      ]
    };
    describe('#addSelfServiceAuthorizedKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSelfServiceAuthorizedKey(sshAddSelfServiceAuthorizedKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'addSelfServiceAuthorizedKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshAddSelfServicePrivateKeyBodyParam = {
      Owner: 'string',
      ContactEmail: 'string',
      FolderId: 'string',
      Location: 'string',
      Notes: 'string',
      KeysetId: 'string'
    };
    describe('#addSelfServicePrivateKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSelfServicePrivateKey(sshAddSelfServicePrivateKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'addSelfServicePrivateKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshAddUserPrivateKeyBodyParam = {
      DeviceGuid: 'string',
      Username: 'string',
      Filepath: 'string',
      KeysetId: 'string',
      Format: 'string',
      Passphrase: 'string'
    };
    describe('#addUserPrivateKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addUserPrivateKey(sshAddUserPrivateKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'addUserPrivateKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshApproveKeyOperationBodyParam = {
      KeyId: 'string',
      Comment: 'string'
    };
    describe('#approveKeyOperation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.approveKeyOperation(sshApproveKeyOperationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'approveKeyOperation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshCancelKeyOperationBodyParam = {
      KeyId: 'string'
    };
    describe('#cancelKeyOperation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelKeyOperation(sshCancelKeyOperationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'cancelKeyOperation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshCancelRotationBodyParam = {
      KeysetId: 'string',
      Options: 9
    };
    describe('#cancelRotation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelRotation(sshCancelRotationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'cancelRotation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshChangePrivateKeyPassphraseBodyParam = {
      KeyId: 7,
      Passphrase: 'string'
    };
    describe('#changePrivateKeyPassphrase - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changePrivateKeyPassphrase(sshChangePrivateKeyPassphraseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'changePrivateKeyPassphrase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshConfirmSelfServiceKeyInstallationBodyParam = {
      KeysetId: 'string',
      Options: 2
    };
    describe('#confirmSelfServiceKeyInstallation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.confirmSelfServiceKeyInstallation(sshConfirmSelfServiceKeyInstallationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'confirmSelfServiceKeyInstallation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshDeleteUnmatchedKeysetBodyParam = {
      UnmatchedTrustId: 'string'
    };
    describe('#deleteUnmatchedKeyset - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUnmatchedKeyset(sshDeleteUnmatchedKeysetBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'deleteUnmatchedKeyset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshGetDevicesBodyParam = {
      SshDeviceFilter: {
        DeviceName: [
          'string'
        ],
        Type: 'string',
        IsCompliant: false
      },
      PageSize: 6,
      Offset: 2
    };
    describe('#getDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDevices(sshGetDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'getDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshEditKeyOptionsBodyParam = {
      KeyId: 'string',
      AllowedSourceRestriction: [
        'string'
      ],
      DeniedSourceRestriction: [
        'string'
      ],
      ForcedCommand: 'string',
      Options: [
        'string'
      ]
    };
    describe('#editKeyOptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editKeyOptions(sshEditKeyOptionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'editKeyOptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshEditSelfServiceAuthorizedKeyBodyParam = {
      Location: 'string',
      Notes: 'string',
      AllowedSourceRestriction: [
        'string'
      ],
      DeniedSourceRestriction: [
        'string'
      ],
      ForcedCommand: 'string',
      Options: [
        'string'
      ],
      KeyId: 'string'
    };
    describe('#editSelfServiceAuthorizedKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editSelfServiceAuthorizedKey(sshEditSelfServiceAuthorizedKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'editSelfServiceAuthorizedKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshExportSelfServiceAuthorizedKeyBodyParam = {
      KeyId: 4,
      Format: 'string'
    };
    describe('#exportSelfServiceAuthorizedKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportSelfServiceAuthorizedKey(sshExportSelfServiceAuthorizedKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'exportSelfServiceAuthorizedKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshExportSelfServicePrivateKeyBodyParam = {
      Passphrase: 'string',
      KeyId: 10,
      Format: 'string'
    };
    describe('#exportSelfServicePrivateKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportSelfServicePrivateKey(sshExportSelfServicePrivateKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'exportSelfServicePrivateKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshImportAuthorizedKeyBodyParam = {
      KeyContentBase64: 'string',
      DeviceGuid: 'string',
      Username: 'string',
      Filepath: 'string',
      Format: 'string'
    };
    describe('#importAuthorizedKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importAuthorizedKey(sshImportAuthorizedKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'importAuthorizedKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshImportKeyUsageDataBodyParam = {
      LogData: [
        {
          LogRecord: 'string',
          LogUtcEpochDate: 'string'
        }
      ]
    };
    describe('#importKeyUsageData - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importKeyUsageData(sshImportKeyUsageDataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'importKeyUsageData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshImportPrivateKeyBodyParam = {
      KeyContentBase64: 'string',
      DeviceGuid: 'string',
      Username: 'string',
      Passphrase: 'string',
      Filepath: 'string',
      Format: 'string'
    };
    describe('#importPrivateKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importPrivateKey(sshImportPrivateKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'importPrivateKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshGetKeyDetailsBodyParam = {
      KeyId: [
        10
      ]
    };
    describe('#getKeyDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getKeyDetails(sshGetKeyDetailsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'getKeyDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshGetKeyUsagesBodyParam = {
      SshKeyUsageFilter: {
        ServerName: [
          'string'
        ],
        ServerAccount: [
          'string'
        ],
        ClientName: [
          'string'
        ],
        Alert: [
          1
        ]
      },
      PageSize: 10,
      Offset: 4
    };
    describe('#getKeyUsages - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getKeyUsages(sshGetKeyUsagesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'getKeyUsages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshGetKeysetDetailsBodyParam = {
      KeysetFilter: {
        DeviceGuid: [
          'string'
        ],
        Username: [
          'string'
        ],
        Length: [
          3
        ],
        MaxKeyLength: 4,
        Algorithm: [
          'string'
        ],
        ProcessStatus: [
          9
        ],
        ViolationStatus: [
          10
        ],
        Type: 'string',
        UsageFilterType: 'string',
        LastUsed: 'string',
        AuthorizedKeyComment: [
          'string'
        ],
        FingerprintSHA256: [
          'string'
        ],
        FingerprintMD5: [
          'string'
        ]
      },
      PageSize: 1,
      Offset: 3,
      LoadKeyData: true
    };
    describe('#getKeysetDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getKeysetDetails(sshGetKeysetDetailsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'getKeysetDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshMoveKeysetsToPolicyBodyParam = {
      KeysetIds: [
        'string'
      ],
      PolicyPath: 'string'
    };
    describe('#moveKeysetsToPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.moveKeysetsToPolicy(sshMoveKeysetsToPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'moveKeysetsToPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshRejectKeyOperationBodyParam = {
      KeyId: 'string',
      Comment: 'string'
    };
    describe('#rejectKeyOperation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rejectKeyOperation(sshRejectKeyOperationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'rejectKeyOperation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshRemoveBodyParam = {
      KeyId: 'string'
    };
    describe('#remove - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.remove(sshRemoveBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'remove', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshRetryKeyOperationBodyParam = {
      KeyId: 'string'
    };
    describe('#retryKeyOperation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retryKeyOperation(sshRetryKeyOperationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'retryKeyOperation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshRetryRotationBodyParam = {
      KeysetId: 'string',
      Options: 4
    };
    describe('#retryRotation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retryRotation(sshRetryRotationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'retryRotation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshStartRotationBodyParam = {
      KeysetId: 'string',
      Options: 3
    };
    describe('#startRotation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.startRotation(sshStartRotationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'startRotation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshSetUnmatchedKeysetPasshraseBodyParam = {
      UnmatchedTrustId: 'string',
      Passphrase: 'string'
    };
    describe('#setUnmatchedKeysetPasshrase - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setUnmatchedKeysetPasshrase(sshSetUnmatchedKeysetPasshraseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'setUnmatchedKeysetPasshrase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshSkipKeyRotationBodyParam = {
      KeyId: 'string'
    };
    describe('#skipKeyRotation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.skipKeyRotation(sshSkipKeyRotationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'skipKeyRotation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshTestDeviceConnectionBodyParam = {
      deviceGuids: [
        'string'
      ]
    };
    describe('#testDeviceConnection - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.testDeviceConnection(sshTestDeviceConnectionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'testDeviceConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshKeysetId = 'fakedata';
    const sshLoadKeyData = true;
    describe('#getKeysetDetail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getKeysetDetail(sshKeysetId, sshLoadKeyData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'getKeysetDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshMinAllowedKeyLength = 555;
    describe('#criticalAlerts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.criticalAlerts(sshMinAllowedKeyLength, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'criticalAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#policyViolations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.policyViolations((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'policyViolations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshGroupBy = 'fakedata';
    describe('#stats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.stats(sshGroupBy, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssh', 'stats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshcertificatesCreateNewCAKeypairBodyParam = {
      Name: 'string',
      ParentDN: 'string',
      KeyAlgorithm: 'string',
      KeyStorage: 'string',
      PrivateKeyData: 'string',
      PrivateKeyPassphrase: 'string'
    };
    describe('#createNewCAKeypair - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNewCAKeypair(sshcertificatesCreateNewCAKeypairBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sshcertificates', 'createNewCAKeypair', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshcertificatesRequestCertificateBodyParam = {
      CADN: 'string',
      PolicyDN: 'string',
      ObjectName: 'string',
      DestinationAddress: 'string',
      Origin: 'string',
      RequestedBy: 'string',
      OriginatingIP: 'string',
      KeyId: 'string',
      Principals: [
        'string'
      ],
      ValidityPeriod: 'string',
      PublicKeyData: 'string',
      Extensions: {},
      ForceCommand: 'string',
      SourceAddresses: [
        'string'
      ],
      PrivateKeyPassphrase: 'string',
      IncludePrivateKeyData: true,
      IncludeCertificateDetails: false,
      ProcessingTimeout: 1
    };
    describe('#requestCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.requestCertificate(sshcertificatesRequestCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sshcertificates', 'requestCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshcertificatesRetrieveCertificateBodyParam = {
      PrivateKeyPassphrase: 'string',
      IncludePrivateKeyData: false,
      IncludeCertificateDetails: true,
      DN: 'string',
      Guid: 'string'
    };
    describe('#retrieveCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveCertificate(sshcertificatesRetrieveCertificateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sshcertificates', 'retrieveCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshcertificatesRetrieveTemplateBodyParam = {
      IncludeCAKeyPairDetails: true,
      DN: 'string',
      Guid: 'string'
    };
    describe('#retrieveTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveTemplate(sshcertificatesRetrieveTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sshcertificates', 'retrieveTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvailableTemplates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAvailableTemplates((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sshcertificates', 'getAvailableTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sshcertificatesGuid = 'fakedata';
    const sshcertificatesDn = 'fakedata';
    describe('#retrieveCAPublicKeyData - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveCAPublicKeyData(sshcertificatesGuid, sshcertificatesDn, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sshcertificates', 'retrieveCAPublicKeyData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsCreateTeamBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 4,
        Disabled: false
      },
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 7,
        Disabled: false
      },
      Team: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 1,
        Disabled: false
      },
      NewTeamName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 3,
          Disabled: true
        }
      ],
      ShowMembers: true,
      Owners: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 6,
          Disabled: false
        }
      ],
      Products: [
        'string'
      ],
      Assets: [
        'string'
      ],
      Description: 'string'
    };
    describe('#createTeam - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTeam(teamsCreateTeamBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'createTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsAddTeamMembersBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 2,
        Disabled: true
      },
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 5,
        Disabled: false
      },
      Team: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 6,
        Disabled: true
      },
      NewTeamName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 3,
          Disabled: false
        }
      ],
      ShowMembers: true,
      Owners: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 4,
          Disabled: false
        }
      ],
      Products: [
        'string'
      ],
      Assets: [
        'string'
      ],
      Description: 'string'
    };
    describe('#addTeamMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addTeamMembers(teamsAddTeamMembersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'addTeamMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsAddTeamOwnersBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 2,
        Disabled: true
      },
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 7,
        Disabled: false
      },
      Team: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 2,
        Disabled: true
      },
      NewTeamName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 3,
          Disabled: true
        }
      ],
      ShowMembers: false,
      Owners: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 5,
          Disabled: true
        }
      ],
      Products: [
        'string'
      ],
      Assets: [
        'string'
      ],
      Description: 'string'
    };
    describe('#addTeamOwners - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addTeamOwners(teamsAddTeamOwnersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'addTeamOwners', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsDemoteTeamOwnersBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 8,
        Disabled: true
      },
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 2,
        Disabled: false
      },
      Team: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 7,
        Disabled: false
      },
      NewTeamName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 6,
          Disabled: false
        }
      ],
      ShowMembers: true,
      Owners: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 3,
          Disabled: true
        }
      ],
      Products: [
        'string'
      ],
      Assets: [
        'string'
      ],
      Description: 'string'
    };
    describe('#demoteTeamOwners - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.demoteTeamOwners(teamsDemoteTeamOwnersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'demoteTeamOwners', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsRemoveTeamMembersBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 2,
        Disabled: true
      },
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 8,
        Disabled: false
      },
      Team: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 3,
        Disabled: false
      },
      NewTeamName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 2,
          Disabled: true
        }
      ],
      ShowMembers: true,
      Owners: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 2,
          Disabled: false
        }
      ],
      Products: [
        'string'
      ],
      Assets: [
        'string'
      ],
      Description: 'string'
    };
    describe('#removeTeamMembers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeTeamMembers(teamsRemoveTeamMembersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'removeTeamMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsRenameTeamBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 2,
        Disabled: false
      },
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 2,
        Disabled: false
      },
      Team: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: true,
        Type: 5,
        Disabled: true
      },
      NewTeamName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 3,
          Disabled: false
        }
      ],
      ShowMembers: true,
      Owners: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 3,
          Disabled: false
        }
      ],
      Products: [
        'string'
      ],
      Assets: [
        'string'
      ],
      Description: 'string'
    };
    describe('#renameTeam - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.renameTeam(teamsRenameTeamBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'renameTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsPrefix = 'fakedata';
    const teamsPrincipal = 'fakedata';
    const teamsUpdateTeamBodyParam = {
      ID: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 2,
        Disabled: true
      },
      Name: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 8,
        Disabled: true
      },
      Team: {
        Prefix: 'string',
        PrefixedName: 'string',
        PrefixedUniversal: 'string',
        Name: 'string',
        FullName: 'string',
        Universal: 'string',
        IsGroup: false,
        Type: 7,
        Disabled: true
      },
      NewTeamName: 'string',
      Members: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 1,
          Disabled: true
        }
      ],
      ShowMembers: false,
      Owners: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 2,
          Disabled: true
        }
      ],
      Products: [
        'string'
      ],
      Assets: [
        'string'
      ],
      Description: 'string'
    };
    describe('#updateTeam - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateTeam(teamsUpdateTeamBodyParam, teamsPrefix, teamsPrincipal, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'updateTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeam - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTeam(teamsPrefix, teamsPrincipal, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowTicketCreateBodyParam = {
      WorkflowDN: 'string',
      Approvers: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 10,
          Disabled: true
        }
      ],
      GUID: 'string',
      Status: 'string',
      Explanation: 'string',
      ObjectDN: 'string',
      Reason: 8,
      UserData: 'string',
      ScheduledStart: 'string',
      ScheduledStop: 'string'
    };
    describe('#ticketCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ticketCreate(workflowTicketCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflow', 'ticketCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowTicketDeleteBodyParam = {
      WorkflowDN: 'string',
      Approvers: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 9,
          Disabled: false
        }
      ],
      GUID: 'string',
      Status: 'string',
      Explanation: 'string',
      ObjectDN: 'string',
      Reason: 3,
      UserData: 'string',
      ScheduledStart: 'string',
      ScheduledStop: 'string'
    };
    describe('#ticketDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ticketDelete(workflowTicketDeleteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflow', 'ticketDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowTicketDetailsBodyParam = {
      WorkflowDN: 'string',
      Approvers: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 8,
          Disabled: false
        }
      ],
      GUID: 'string',
      Status: 'string',
      Explanation: 'string',
      ObjectDN: 'string',
      Reason: 1,
      UserData: 'string',
      ScheduledStart: 'string',
      ScheduledStop: 'string'
    };
    describe('#ticketDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ticketDetails(workflowTicketDetailsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflow', 'ticketDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowTicketEnumerateBodyParam = {
      WorkflowDN: 'string',
      Approvers: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 6,
          Disabled: false
        }
      ],
      GUID: 'string',
      Status: 'string',
      Explanation: 'string',
      ObjectDN: 'string',
      Reason: 3,
      UserData: 'string',
      ScheduledStart: 'string',
      ScheduledStop: 'string'
    };
    describe('#ticketEnumerate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ticketEnumerate(workflowTicketEnumerateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflow', 'ticketEnumerate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowTicketExistsBodyParam = {
      WorkflowDN: 'string',
      Approvers: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 7,
          Disabled: false
        }
      ],
      GUID: 'string',
      Status: 'string',
      Explanation: 'string',
      ObjectDN: 'string',
      Reason: 4,
      UserData: 'string',
      ScheduledStart: 'string',
      ScheduledStop: 'string'
    };
    describe('#ticketExists - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ticketExists(workflowTicketExistsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflow', 'ticketExists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowTicketStatusBodyParam = {
      WorkflowDN: 'string',
      Approvers: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: false,
          Type: 7,
          Disabled: false
        }
      ],
      GUID: 'string',
      Status: 'string',
      Explanation: 'string',
      ObjectDN: 'string',
      Reason: 9,
      UserData: 'string',
      ScheduledStart: 'string',
      ScheduledStop: 'string'
    };
    describe('#ticketStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ticketStatus(workflowTicketStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflow', 'ticketStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowTicketUpdateStatusBodyParam = {
      WorkflowDN: 'string',
      Approvers: [
        {
          Prefix: 'string',
          PrefixedName: 'string',
          PrefixedUniversal: 'string',
          Name: 'string',
          FullName: 'string',
          Universal: 'string',
          IsGroup: true,
          Type: 2,
          Disabled: true
        }
      ],
      GUID: 'string',
      Status: 'string',
      Explanation: 'string',
      ObjectDN: 'string',
      Reason: 6,
      UserData: 'string',
      ScheduledStart: 'string',
      ScheduledStop: 'string'
    };
    describe('#ticketUpdateStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ticketUpdateStatus(workflowTicketUpdateStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflow', 'ticketUpdateStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const preferencesAddBodyParam = {
      Preferences: [
        {
          Id: 5,
          Universal: 'string',
          Product: 'string',
          Category: 'string',
          Name: 'string',
          Value: 'string',
          Locked: true
        }
      ]
    };
    describe('#add - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.add(preferencesAddBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Preferences', 'add', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#get3 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.get3(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Preferences', 'get3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#certificateDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.certificateDelete(certificatesGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Certificates', 'certificateDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete2(credentialsGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'delete2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const discoveryGuid = 'fakedata';
    describe('#discoveryDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.discoveryDelete(discoveryGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Discovery', 'discoveryDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityPrefix = 'fakedata';
    const identityPrincipal = 'fakedata';
    describe('#deleteGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGroup(identityPrefix, identityPrincipal, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Identity', 'deleteGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete3 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete3(pkiHashicorpGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PkiHashicorp', 'delete3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete4 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delete4(pkiHashicorpGuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PkiHashicorp', 'delete4', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTeam - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTeam(teamsPrefix, teamsPrincipal, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'deleteTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clear - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.clear(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-venafi_trust_protection_platform-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Preferences', 'clear', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
