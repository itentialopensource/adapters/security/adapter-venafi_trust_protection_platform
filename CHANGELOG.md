
## 0.3.6 [10-15-2024]

* Changes made at 2024.10.14_19:58PM

See merge request itentialopensource/adapters/adapter-venafi_trust_protection_platform!13

---

## 0.3.5 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-venafi_trust_protection_platform!11

---

## 0.3.4 [08-14-2024]

* Changes made at 2024.08.14_18:07PM

See merge request itentialopensource/adapters/adapter-venafi_trust_protection_platform!10

---

## 0.3.3 [08-06-2024]

* Changes made at 2024.08.06_19:20PM

See merge request itentialopensource/adapters/adapter-venafi_trust_protection_platform!9

---

## 0.3.2 [05-23-2024]

* Patch/adapt 3445

See merge request itentialopensource/adapters/adapter-venafi_trust_protection_platform!8

---

## 0.3.1 [05-21-2024]

* Remove trailing slash

See merge request itentialopensource/adapters/adapter-venafi_trust_protection_platform!7

---

## 0.3.0 [05-14-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-venafi_trust_protection_platform!6

---

## 0.2.4 [04-19-2024]

* Add trailing slash in paths

See merge request itentialopensource/adapters/security/adapter-venafi_trust_protection_platform!5

---

## 0.2.3 [03-27-2024]

* Changes made at 2024.03.27_13:38PM

See merge request itentialopensource/adapters/security/adapter-venafi_trust_protection_platform!4

---

## 0.2.2 [03-11-2024]

* Changes made at 2024.03.11_16:12PM

See merge request itentialopensource/adapters/security/adapter-venafi_trust_protection_platform!3

---

## 0.2.1 [02-27-2024]

* Changes made at 2024.02.27_11:45AM

See merge request itentialopensource/adapters/security/adapter-venafi_trust_protection_platform!2

---

## 0.2.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-venafi_trust_protection_platform!1

---

## 0.1.1 [07-25-2022]

* Bug fixes and performance improvements

See commit 84269bd

---
