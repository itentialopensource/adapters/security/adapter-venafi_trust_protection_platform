## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Venafi Trust Protection Platform. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Venafi Trust Protection Platform.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the venafi_trust_protection_platform. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">requestDeviceAuth(request, callback)</td>
    <td style="padding:15px">Initiates a device authorization flow (OAuth 2.0 Device Authorization Grant).</td>
    <td style="padding:15px">{base_path}/{version}/vedauth/authorize/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authorizeRoot(request, callback)</td>
    <td style="padding:15px">Obtains an authorization token.</td>
    <td style="padding:15px">{base_path}/{version}/vedauth/authorize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authorizeOAuth(request, callback)</td>
    <td style="padding:15px">Obtains an authorization token.</td>
    <td style="padding:15px">{base_path}/{version}/vedauth/authorize/oauth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">integratedAuthorize(request, callback)</td>
    <td style="padding:15px">Authenticates a user with Integrated Windows Authentication.</td>
    <td style="padding:15px">{base_path}/{version}/vedauth/authorize/integrated?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateAuthorize(request, callback)</td>
    <td style="padding:15px">Obtains an authorization token from the Trust Protection Platform server using a client certificate</td>
    <td style="padding:15px">{base_path}/{version}/vedauth/authorize/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">token(request, callback)</td>
    <td style="padding:15px">Refreshes a bearer token. Also supports device access token request (OAuth 2.0 Device Authorization</td>
    <td style="padding:15px">{base_path}/{version}/vedauth/authorize/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyToken(callback)</td>
    <td style="padding:15px">Verifies whether the caller's bearer token is still valid.</td>
    <td style="padding:15px">{base_path}/{version}/vedauth/authorize/verify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeToken(callback)</td>
    <td style="padding:15px">Revokes the caller's Token Auth grant and blocks the ability to make Web SDK calls.</td>
    <td style="padding:15px">{base_path}/{version}/vedauth/revoke/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificates(offset, limit, optionalfields, createdon, createdongreater, createdonless, parentdn, parentdnrecursive, keysize, keysizegreater, keysizeless, stage, stagegreater, stageless, validto, validtogreater, validtoless, validfrom, validfromgreater, validfromless, pendingworkflow, validationstate, inerror, disabled, name, managementtype, validationdisabled, networkvalidationdisabled, certificatetype, cn, c, s, l, o, issuer, serial, keyalgorithm, signaturealgorithm, ou, sanDns, sanIp, sanEmail, sanUri, sanUpn, thumbprint, contact, approver, isselfsigned, iswildcard, ssltlsprotocol, tlsvalidationfailure, chainvalidationfailure, callback)</td>
    <td style="padding:15px">Returns certificate details and the total number of certificates that match the specified search fi</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateCount(createdon, createdongreater, createdonless, parentdn, parentdnrecursive, keysize, keysizegreater, keysizeless, stage, stagegreater, stageless, validto, validtogreater, validtoless, validfrom, validfromgreater, validfromless, pendingworkflow, validationstate, inerror, disabled, name, managementtype, validationdisabled, networkvalidationdisabled, certificatetype, cn, c, s, l, o, issuer, serial, keyalgorithm, signaturealgorithm, ou, sanDns, sanIp, sanEmail, sanUri, sanUpn, thumbprint, contact, approver, isselfsigned, iswildcard, ssltlsprotocol, tlsvalidationfailure, chainvalidationfailure, callback)</td>
    <td style="padding:15px">Searches and returns the number of certificates that match the specified search filters.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateDetails(guid, callback)</td>
    <td style="padding:15px">Retrieves details and validation information about a certificate.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificatePut(guid, request, callback)</td>
    <td style="padding:15px">Sets or clears one or more Config attributes of a X509 certificate.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateDelete(guid, callback)</td>
    <td style="padding:15px">Removes a Certificate object, all associated objects including pending workflow tickets, and the co</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reset(request, callback)</td>
    <td style="padding:15px">Resets the state of a certificate and its associated applications.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retry(request, callback)</td>
    <td style="padding:15px">Allows the Application object to retry the last unsuccessful lifecycle stage of a particular certif</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Retry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getValidationResults(guid, callback)</td>
    <td style="padding:15px">Retrieves the list of validation results for the certificate.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/{pathv1}/ValidationResults?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateRetrieveByVaultId(vaultId, request, callback)</td>
    <td style="padding:15px">Returns the available certificate data and optional private key information for an enrolled or arch</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Retrieve/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateRetrieveByVaultIdGet(vaultId, format, password, includePrivateKey, includeChain, rootFirstOrder, friendlyName, keystorePassword, callback)</td>
    <td style="padding:15px">Returns the available certificate data and optional private key information for an enrolled or arch</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Retrieve/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificatePreviousVersions(guid, excludeExpired, excludeRevoked, callback)</td>
    <td style="padding:15px">Gets the certificate previous versions.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/{pathv1}/PreviousVersions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateValidate(request, callback)</td>
    <td style="padding:15px">Initiates SSL/TLS network validation for one or more certificates and any associated applications.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkPolicy(request, callback)</td>
    <td style="padding:15px">Returns the policy and a compliance assessment for a Certificate Signing Request (CSR).</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/CheckPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateRequest(request, callback)</td>
    <td style="padding:15px">Enrolls or provisions a new certificate.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateRetrieve(request, callback)</td>
    <td style="padding:15px">Returns the available certificate data and optional private key information for an enrolled certifi</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Retrieve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateRetrieveGet(certificateDN, format, password, includePrivateKey, includeChain, rootFirstOrder, friendlyName, keystorePassword, workToDoTimeout, callback)</td>
    <td style="padding:15px">Returns the available certificate data and optional private key information for an enrolled certifi</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Retrieve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateRenew(request, callback)</td>
    <td style="padding:15px">Requests immediate renewal for an existing certificate.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateRevoke(request, callback)</td>
    <td style="padding:15px">Sends a revocation request to the certificate CA.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateAssociate(request, callback)</td>
    <td style="padding:15px">Allows one or more Application objects of devices to use an existing certificate.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Associate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateDissociate(request, callback)</td>
    <td style="padding:15px">Detaches one or more Application objects and corresponding Device objects from an existing certific</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Dissociate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificatePush(request, callback)</td>
    <td style="padding:15px">Initiates the certificate provisioning to some or all certificate consumers.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">certificateImport(request, callback)</td>
    <td style="padding:15px">Adds or replaces a Privacy-enhanced Electronic Mail (PEM) certificate based on the reconcile option.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/certificates/Import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create(request, callback)</td>
    <td style="padding:15px">Creates a new credential object. There is no data validation, except for Amazon credentials with AD</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete(request, callback)</td>
    <td style="padding:15px">Deletes an existing credential.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enumerate(request, callback)</td>
    <td style="padding:15px">Returns existing credentials.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/enumerate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rename(request, callback)</td>
    <td style="padding:15px">Renames a credential.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/rename?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieve(request, callback)</td>
    <td style="padding:15px">Returns a credential.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/retrieve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update(request, callback)</td>
    <td style="padding:15px">Updates the fields of an existing credential.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create2(request, callback)</td>
    <td style="padding:15px">Creates an entry in the policy folder to match credential information in a CyberArk Safe.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/CyberArk/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update2(request, callback)</td>
    <td style="padding:15px">Updates credentials to match credential information in a CyberArk Safe.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/CyberArk/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create3(request, callback)</td>
    <td style="padding:15px">Creates an Adaptable Credential, which is a reference to a vault secret.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/Adaptable/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update3(request, callback)</td>
    <td style="padding:15px">Updates field values in an Adaptable Credential.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/Adaptable/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create4(connectorRequest, callback)</td>
    <td style="padding:15px">Creates an Adaptable Credential Connector in VCC.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/Connector/Adaptable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">read(guid, callback)</td>
    <td style="padding:15px">Returns information about an Adaptable Credential Connector that is in VCC.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/Connector/Adaptable/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update4(guid, connectorRequest, callback)</td>
    <td style="padding:15px">Updates an Adaptable Credential Connector in VCC.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/Connector/Adaptable/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete2(guid, callback)</td>
    <td style="padding:15px">Removes an Adaptable Credential Connector from VCC.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/credentials/Connector/Adaptable/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDiscoveryResults(request, callback)</td>
    <td style="padding:15px">Adds or updates a set of network certificates into the Policy tree.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/discovery/Import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveryDelete(guid, callback)</td>
    <td style="padding:15px">Deletes a Network Discovery job including all stored results.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/discovery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">self(callback)</td>
    <td style="padding:15px">Returns the Web SDK caller's self identity and all identities associated with the authenticated ide</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/identity/self?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">browse(request, callback)</td>
    <td style="padding:15px">Returns information about individual identity, group identity, or distribution groups from a local</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/identity/browse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssociatedEntries(request, callback)</td>
    <td style="padding:15px">Returns all associated identity groups and folders for a given identity entry.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/identity/getassociatedentries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMembers(request, callback)</td>
    <td style="padding:15px">Returns information about the members of an identity group.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/identity/getmembers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMemberships(request, callback)</td>
    <td style="padding:15px">Returns information about a user's membership from an identity provider.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/identity/getmemberships?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readAttribute(request, callback)</td>
    <td style="padding:15px">Returns an array of attributes for the requested person or group's identity.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/identity/readattribute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validate(request, callback)</td>
    <td style="padding:15px">Validates and returns missing information about an identity.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/identity/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setIdentityPassword(request, callback)</td>
    <td style="padding:15px">Rotates a password for only a local account. Password rotation for a group account is not supported.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/identity/SetPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGroup(request, callback)</td>
    <td style="padding:15px">Adds a group to an identity provider. If members for the group are provided in the request, they wi</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/identity/addgroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renameGroup(request, callback)</td>
    <td style="padding:15px">Renames a group in an identity provider.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/identity/renamegroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroup(prefix, principal, callback)</td>
    <td style="padding:15px">Deletes a group from an identity provider.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/identity/group/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGroupMembers(request, callback)</td>
    <td style="padding:15px">Adds a member to a group in an identity provider.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/identity/addgroupmembers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeGroupMembers(request, callback)</td>
    <td style="padding:15px">Removes a member from a group in an identity provider.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/identity/removegroupmembers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogsForObject(guid, callback)</td>
    <td style="padding:15px">Returns events information from the Log server based on the GUID of the Distinguished Name.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/log/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogs(callback)</td>
    <td style="padding:15px">Returns event information from the Log server.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enumerateObjectsFilteredRecursive(request, callback)</td>
    <td style="padding:15px">Reports an event to the Log server.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/log/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleGetLogSchema(callback)</td>
    <td style="padding:15px">Gets the log event definitions, with customizations, and the log application definitions defined in</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/log/LogSchema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enumerateTickets(request, callback)</td>
    <td style="padding:15px">Enumerate pending tickets.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/flow/tickets/enumerate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enumerateApproved(request, callback)</td>
    <td style="padding:15px">Enumerate approved and still in use tickets.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/flow/tickets/enumerateapproved?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">approve(request, callback)</td>
    <td style="padding:15px">Approve one or more pending tickets.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/flow/tickets/approve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reject(request, callback)</td>
    <td style="padding:15px">Reject one or more pending tickets.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/flow/tickets/reject?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">count(request, callback)</td>
    <td style="padding:15px">Count pending tickets.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/flow/tickets/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countApproved(request, callback)</td>
    <td style="padding:15px">Count approved tickets.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/flow/tickets/countapproved?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadSingle(request, callback)</td>
    <td style="padding:15px">Load a single ticket</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/flow/tickets/load?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExisting(request, callback)</td>
    <td style="padding:15px">Update existing approval</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/flow/tickets/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">set(request, callback)</td>
    <td style="padding:15px">Assigns a Custom Field value to an object.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/set?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setPolicy(request, callback)</td>
    <td style="padding:15px">Assigns a Custom Field value to a policy.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/setpolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get(request, callback)</td>
    <td style="padding:15px">Returns the values for all Custom Fields that apply to a particular object.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readEffective(request, callback)</td>
    <td style="padding:15px">Returns the effective value of a Custom Field for a particular object.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/readeffectivevalues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readPolicy(request, callback)</td>
    <td style="padding:15px">Returns a Custom Field value that has been applied to a policy for inheritance by objects of a spec</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/readpolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getItems(request, callback)</td>
    <td style="padding:15px">Returns an array of Custom Field attributes that apply to the specified object.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/getitems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyItems(request, callback)</td>
    <td style="padding:15px">Returns policy-assigned values for Custom Fields.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/getpolicyitems?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getItemsForClass(request, callback)</td>
    <td style="padding:15px">Returns an array of Custom Fields and their attributes that apply to the specified object class.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/getitemsforclass?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getItemGuids(request, callback)</td>
    <td style="padding:15px">Returns an array of GUIDs for all of the Custom Fields that apply to the specified object.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/getitemguids?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">find(request, callback)</td>
    <td style="padding:15px">Returns all objects with a value stored for a particular Custom Field.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/find?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">defineItem(request, callback)</td>
    <td style="padding:15px">Creates a new Custom Field to allow user input.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/defineitem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">undefineItem(request, callback)</td>
    <td style="padding:15px">Removes an existing Custom Field.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/undefineitem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findItem(request, callback)</td>
    <td style="padding:15px">Returns the Custom Field settings based on the UI label.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/finditem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadItem(request, callback)</td>
    <td style="padding:15px">Returns a Metadata Item with Custom Field details.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/loaditem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadItemGuid(request, callback)</td>
    <td style="padding:15px">Returns an array of GUIDs for all of the Custom Fields that apply to the specified object.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/loaditemguid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateItem(request, callback)</td>
    <td style="padding:15px">Updates settings that control a Custom Field, such as default and required user input values.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/updateitem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllItems(callback)</td>
    <td style="padding:15px">Returns every Custom Field.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/metadata/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enforceRoles(guid, callback)</td>
    <td style="padding:15px">Creates or updates roles in the HashiCorp Vault PKI secrets engine according to the Trust Protectio</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/pki/hashicorp/ca/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getByGuid(guid, callback)</td>
    <td style="padding:15px">Retrieves details about a HashiCorp Vault PKI secrets engine.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/pki/hashicorp/ca/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update5(guid, request, callback)</td>
    <td style="padding:15px">Updates the configuration for a HashiCorp Vault PKI secrets engine that is managed by Trust Protect</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/pki/hashicorp/ca/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete3(guid, callback)</td>
    <td style="padding:15px">Removes the configuration for managing a HashiCorp Vault PKI secrets engine from Trust Protection P</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/pki/hashicorp/ca/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enrollAndProvision(guid, callback)</td>
    <td style="padding:15px">Initiates the renewal or first time enrollment of a HashiCorp Vault PKI intermediate CA certificate.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/pki/hashicorp/ca/{pathv1}/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create5(request, callback)</td>
    <td style="padding:15px">Defines the configuration for Trust Protection Platform to manage the intermediate CA certificate.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/pki/hashicorp/ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get2(callback)</td>
    <td style="padding:15px">Lists all HashiCorp Vault PKI secrets engines that are managed by Trust Protection Platform.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/pki/hashicorp/ca?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create6(request, callback)</td>
    <td style="padding:15px">Creates a role in Trust Protection Platform.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/pki/hashicorp/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete4(guid, callback)</td>
    <td style="padding:15px">Removes the policy enforcement settings for a HashiCorp Vault PKI role from Trust Protection Platfo</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/pki/hashicorp/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getByGuid2(guid, callback)</td>
    <td style="padding:15px">Retrieves policy information from Trust Protection Platform about a HashiCorp Vault PKI role.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/pki/hashicorp/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update6(guid, request, callback)</td>
    <td style="padding:15px">Updates Trust Protection Platform information about a HashiCorp role.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/pki/hashicorp/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKeysetDetail(keysetId, loadKeyData, callback)</td>
    <td style="padding:15px">Finds information about every device that shares the same keyset.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/KeysetDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKeysetDetails(generatedKeysetFilterPageSizeOffsetLoadKeyData, callback)</td>
    <td style="padding:15px">Returns private and public key pairs that identify details about a device.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/KeysetDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testDeviceConnection(generateddeviceGuids, callback)</td>
    <td style="padding:15px">Returns connection status for provided devices</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/TestDeviceConnection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importKeyUsageData(data, callback)</td>
    <td style="padding:15px">Imports key usage from a Unix or Linux host syslog.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/ImportKeyUsageData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKeyUsages(generatedSshKeyUsageFilterPageSizeOffset, callback)</td>
    <td style="padding:15px">Finds all key usage records matching the specified filtering criteria.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/KeyUsage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remove(keyRequest, callback)</td>
    <td style="padding:15px">Marks a key for deletion.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/RemoveKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelKeyOperation(keyRequest, callback)</td>
    <td style="padding:15px">Cancels any running key operation. Can also rollback a key to its original state.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/CancelKeyOperation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retryKeyOperation(keyRequest, callback)</td>
    <td style="padding:15px">Retries any failing operation (addition, removal, editing) on specified key.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/RetryKeyOperation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startRotation(keysetRequest, callback)</td>
    <td style="padding:15px">Starts rotation of specified keyset.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/Rotate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelRotation(keysetRequest, callback)</td>
    <td style="padding:15px">Cancels rotation of the specified keyset if it is running, or rolls back the keyset to the previous</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/CancelRotation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retryRotation(keysetRequest, callback)</td>
    <td style="padding:15px">Retries rotation of specified keyset.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/RetryRotation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editKeyOptions(optionsRequest, callback)</td>
    <td style="padding:15px">Changes Source Restrictions, and Forced commands in an authorized keys file.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/EditKeyOptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSelfServiceAuthorizedKey(request, callback)</td>
    <td style="padding:15px">Changes location, notes, Source Restrictions, and Forced commands in an selfservice authorized keys.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/EditSelfServiceAuthorizedKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSelfServicePrivateKey(request, callback)</td>
    <td style="padding:15px">Assigns an existing keyset to a policy folder.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/AddSelfServicePrivateKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSelfServiceAuthorizedKey(request, callback)</td>
    <td style="padding:15px">Add an authorization key to a keyset</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/AddSelfServiceAuthorizedKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confirmSelfServiceKeyInstallation(keysetRequest, callback)</td>
    <td style="padding:15px">Verifies that a user manually installed a private key.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/ConfirmSelfServiceKeyInstallation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveKeysetsToPolicy(migrationRequest, callback)</td>
    <td style="padding:15px">Moves a list of keysets to a policy folder.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/MoveKeysetsToPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportSelfServiceAuthorizedKey(request, callback)</td>
    <td style="padding:15px">Downloads an authorized key.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/ExportSelfServiceAuthorizedKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportSelfServicePrivateKey(request, callback)</td>
    <td style="padding:15px">Downloads a private key.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/ExportSelfServicePrivateKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">skipKeyRotation(keyRequest, callback)</td>
    <td style="padding:15px">Skip key rotation.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/SkipKeyRotation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserPrivateKey(request, callback)</td>
    <td style="padding:15px">Installs a new user private key on a device.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/AddUserPrivateKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addHostPrivateKey(request, callback)</td>
    <td style="padding:15px">Creates and installs a new private key on a host device.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/AddHostPrivateKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAuthorizedKey(request, callback)</td>
    <td style="padding:15px">Creates an Open SSH Authorized key for provisioning to a device.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/AddAuthorizedKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addKnownHostKey(request, callback)</td>
    <td style="padding:15px">Creates a public key for a host.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/AddKnownHostKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importAuthorizedKey(data, callback)</td>
    <td style="padding:15px">Adds or reuses a Base64 public key for a device.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/ImportAuthorizedKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importPrivateKey(data, callback)</td>
    <td style="padding:15px">Adds or reuses a Base64 private key for a device.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/ImportPrivateKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePrivateKeyPassphrase(data, callback)</td>
    <td style="padding:15px">Changes the private key passphrase for a keyset.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/ChangePrivateKeyPassphrase?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setUnmatchedKeysetPasshrase(data, callback)</td>
    <td style="padding:15px">Sets the passphrase on a keyset that is missing the encrypted private key.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/SetUnmatchedKeysetPassphrase?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUnmatchedKeyset(requestData, callback)</td>
    <td style="padding:15px">Deletes an unmatched keyset that is missing the encrypted private key passphrase and .pub file exte</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/DeleteUnmatchedKeyset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKeyDetails(generatedKeyId, callback)</td>
    <td style="padding:15px">Returns detailed key data about a device.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/KeyDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevices(generatedSshDeviceFilterPageSizeOffset, callback)</td>
    <td style="padding:15px">Finds devices in the Policy tree that match the filtering criteria.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/Devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">approveKeyOperation(approveRequest, callback)</td>
    <td style="padding:15px">Approves any key operation that has a Pending Approval status in a workflow.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/ApproveKeyOperation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rejectKeyOperation(rejectRequest, callback)</td>
    <td style="padding:15px">Rejects any key operation that has a Pending Approval status in a workflow.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/RejectKeyOperation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stats(groupBy, callback)</td>
    <td style="padding:15px">Retrieves data from SSH dashboard widgets that provide SSH statistics.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/widget/Stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">policyViolations(callback)</td>
    <td style="padding:15px">Returns the number of SSH keys that are in violation of Trust Protection Platform folders.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/widget/PolicyViolations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">criticalAlerts(minAllowedKeyLength, callback)</td>
    <td style="padding:15px">Returns counts for items that represent security risk and should be addressed (Critical Alerts).</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/ssh/widget/CriticalAlerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">requestCertificate(request, callback)</td>
    <td style="padding:15px">Requests a new SSH certificate</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/sshcertificates/request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveCertificate(request, callback)</td>
    <td style="padding:15px">Returns the available certificate data and optional private key information for an enrolled SSH cer</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/sshcertificates/retrieve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvailableTemplates(callback)</td>
    <td style="padding:15px">Returns a list of SSH CA templates the current user can request certificates from.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/sshcertificates/template/available?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveTemplate(request, callback)</td>
    <td style="padding:15px">Returns information about an SSH CA template.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/sshcertificates/template/retrieve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveCAPublicKeyData(guid, dn, callback)</td>
    <td style="padding:15px">Returns the configured CA public key.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/sshcertificates/template/retrieve/PublicKeyData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewCAKeypair(request, callback)</td>
    <td style="padding:15px">Create new CA Keypair.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/sshcertificates/CaKeyPair/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeam(prefix, principal, callback)</td>
    <td style="padding:15px">Gets a team from an identity provider.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/teams/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTeam(request, prefix, principal, callback)</td>
    <td style="padding:15px">Updates the existent team.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/teams/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeam(prefix, principal, callback)</td>
    <td style="padding:15px">Deletes a team from an identity provider.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/teams/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTeam(request, callback)</td>
    <td style="padding:15px">Adds a team to an identity provider. If members for the team are provided in the request, they will</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/teams/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTeamMembers(request, callback)</td>
    <td style="padding:15px">Adds members to a team in a local identity provider.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/teams/addteammembers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeTeamMembers(request, callback)</td>
    <td style="padding:15px">Removes members from a team in an identity provider.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/teams/removeteammembers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTeamOwners(request, callback)</td>
    <td style="padding:15px">Adds owners to a team in a local identity provider.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/teams/addteamowners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">demoteTeamOwners(request, callback)</td>
    <td style="padding:15px">Demotes owners at an identity provider team.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/teams/demoteteamowners?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renameTeam(request, callback)</td>
    <td style="padding:15px">Renames a team and updates team assets with new team name</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/teams/renameteam?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ticketCreate(request, callback)</td>
    <td style="padding:15px">Initiates a ticket to proceed through a existing workflow and act on a Policy tree object.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/workflow/ticket/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ticketDelete(request, callback)</td>
    <td style="padding:15px">Permanently removes an existing workflow ticket from the platform.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/workflow/ticket/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ticketDetails(request, callback)</td>
    <td style="padding:15px">Returns the status, list of approvers, and other details about a workflow ticket.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/workflow/ticket/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ticketEnumerate(request, callback)</td>
    <td style="padding:15px">Returns the GUIDs of all workflow tickets associated with the object and user.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/workflow/ticket/enumerate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ticketExists(request, callback)</td>
    <td style="padding:15px">Verifies whether a workflow ticket exists.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/workflow/ticket/exists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ticketStatus(request, callback)</td>
    <td style="padding:15px">Retrieves the status string for an existing workflow ticket.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/workflow/ticket/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ticketUpdateStatus(request, callback)</td>
    <td style="padding:15px">Updates the status of a workflow ticket as approved or rejected and provides the option to set a sc</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/workflow/ticket/updatestatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get3(product, category, name, callback)</td>
    <td style="padding:15px">Gets the specified preferences for this identity.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/preferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">add(request, callback)</td>
    <td style="padding:15px">Adds the specified preferences for the current identity.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/preferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clear(product, category, name, callback)</td>
    <td style="padding:15px">Clears the specified preferences for the current identity.</td>
    <td style="padding:15px">{base_path}/{version}/vedsdk/preferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
