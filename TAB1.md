# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the venafi_trust_protection_platform System. The API that was used to build the adapter for venafi_trust_protection_platform is usually available in the report directory of this adapter. The adapter utilizes the venafi_trust_protection_platform API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Venafi Trust Protection Platform adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Venafi Trust Protection Platform. With this adapter you have the ability to perform operations on items such as:

- Certificates

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
