# venafi_trust_protection_platform

Vendor: Venafi
Homepage: https://venafi.com/

Product: Trust Protection Platform
Product Page: https://docs.venafi.com/

## Introduction
We classify Venafi Trust Protection Platform into the Security (SASE) domain as it provides a Security Trust solutions.

## Why Integrate
The Venafi Trust Protection Platform adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Venafi Trust Protection Platform. With this adapter you have the ability to perform operations on items such as:

- Certificates

## Additional Product Documentation
The [Venafi 24.1 REST API Reference](https://docs.venafi.com/Docs/24.1API/#?route=overview)
The [Venafi Authentication API Reference](https://docs.venafi.com/Docs/current/TopNav/Content/SDK/WebSDK/cco-sdk-GettingStarted.php?tocpath=REST%20APIs%7CGetting%20started%20with%20automation%7C_____0)